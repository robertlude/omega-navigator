/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/background/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/background/index.js":
/*!*********************************!*\
  !*** ./src/background/index.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _messageHandlers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./messageHandlers */ "./src/background/messageHandlers/index.js");
// Internal Dependencies
 // Module

var data = {
  organizations: []
};
chrome.storage.local.get(['organizations'], function (_ref) {
  var organizations = _ref.organizations;
  return data.organizations = organizations || [];
});
chrome.runtime.onMessage.addListener(function (message) {
  console.log('background message', message);
  if (!Object.keys(_messageHandlers__WEBPACK_IMPORTED_MODULE_0__["default"]).includes(message.type)) return;
  _messageHandlers__WEBPACK_IMPORTED_MODULE_0__["default"][message.type](data, message);
  console.log('new background state', data);
});

/***/ }),

/***/ "./src/background/messageHandlers/index.js":
/*!*************************************************!*\
  !*** ./src/background/messageHandlers/index.js ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _synchronize__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./synchronize */ "./src/background/messageHandlers/synchronize/index.js");
/* harmony import */ var _synchronized__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./synchronized */ "./src/background/messageHandlers/synchronized.js");
/* harmony import */ var _messageTypes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/messageTypes */ "./src/messageTypes.js");
var _MESSAGE_TYPE__SYNCHR;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Internal Dependencies


 // Module

/* harmony default export */ __webpack_exports__["default"] = (_MESSAGE_TYPE__SYNCHR = {}, _defineProperty(_MESSAGE_TYPE__SYNCHR, _messageTypes__WEBPACK_IMPORTED_MODULE_2__["MESSAGE_TYPE__SYNCHRONIZE"], _synchronize__WEBPACK_IMPORTED_MODULE_0__["default"]), _defineProperty(_MESSAGE_TYPE__SYNCHR, _messageTypes__WEBPACK_IMPORTED_MODULE_2__["MESSAGE_TYPE__SYNCHRONIZED"], _synchronized__WEBPACK_IMPORTED_MODULE_1__["default"]), _MESSAGE_TYPE__SYNCHR);

/***/ }),

/***/ "./src/background/messageHandlers/synchronize/createUpdater.js":
/*!*********************************************************************!*\
  !*** ./src/background/messageHandlers/synchronize/createUpdater.js ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// Module
/* harmony default export */ __webpack_exports__["default"] = (function (tabId) {
  return function (eventTabId, info) {
    if (tabId != eventTabId) return;
    if (info.status != 'complete') return;
    chrome.tabs.executeScript(tabId, {
      file: 'synchronizeOrchestration.js'
    });
  };
});

/***/ }),

/***/ "./src/background/messageHandlers/synchronize/index.js":
/*!*************************************************************!*\
  !*** ./src/background/messageHandlers/synchronize/index.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _createUpdater__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./createUpdater */ "./src/background/messageHandlers/synchronize/createUpdater.js");
// Internal Dependencies
 // Module

/* harmony default export */ __webpack_exports__["default"] = (function (data, _ref) {
  var hostname = _ref.hostname;
  chrome.tabs.create({
    url: "".concat(hostname, "plans")
  }, function (tab) {
    var updater = Object(_createUpdater__WEBPACK_IMPORTED_MODULE_0__["default"])(tab.id);
    chrome.tabs.onUpdated.addListener(updater);
  });
});

/***/ }),

/***/ "./src/background/messageHandlers/synchronized.js":
/*!********************************************************!*\
  !*** ./src/background/messageHandlers/synchronized.js ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// Module
/* harmony default export */ __webpack_exports__["default"] = (function (data, _ref) {
  var organizations = _ref.organizations;
  data.organizations = organizations;
  chrome.storage.local.set({
    organizations: organizations
  }, function () {
    return console.log('organizations saved');
  });
});

/***/ }),

/***/ "./src/messageTypes.js":
/*!*****************************!*\
  !*** ./src/messageTypes.js ***!
  \*****************************/
/*! exports provided: MESSAGE_TYPE__SET_ORGANIZATIONS, MESSAGE_TYPE__SYNCHRONIZE, MESSAGE_TYPE__SYNCHRONIZED */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MESSAGE_TYPE__SET_ORGANIZATIONS", function() { return MESSAGE_TYPE__SET_ORGANIZATIONS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MESSAGE_TYPE__SYNCHRONIZE", function() { return MESSAGE_TYPE__SYNCHRONIZE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MESSAGE_TYPE__SYNCHRONIZED", function() { return MESSAGE_TYPE__SYNCHRONIZED; });
// Module
var MESSAGE_TYPE__SET_ORGANIZATIONS = 'messageType:setOrganizations',
    MESSAGE_TYPE__SYNCHRONIZE = 'messageType:synchronize',
    MESSAGE_TYPE__SYNCHRONIZED = 'messageType:synchronized';


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2JhY2tncm91bmQvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2JhY2tncm91bmQvbWVzc2FnZUhhbmRsZXJzL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9iYWNrZ3JvdW5kL21lc3NhZ2VIYW5kbGVycy9zeW5jaHJvbml6ZS9jcmVhdGVVcGRhdGVyLmpzIiwid2VicGFjazovLy8uL3NyYy9iYWNrZ3JvdW5kL21lc3NhZ2VIYW5kbGVycy9zeW5jaHJvbml6ZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvYmFja2dyb3VuZC9tZXNzYWdlSGFuZGxlcnMvc3luY2hyb25pemVkLmpzIiwid2VicGFjazovLy8uL3NyYy9tZXNzYWdlVHlwZXMuanMiXSwibmFtZXMiOlsiZGF0YSIsIm9yZ2FuaXphdGlvbnMiLCJjaHJvbWUiLCJzdG9yYWdlIiwibG9jYWwiLCJnZXQiLCJydW50aW1lIiwib25NZXNzYWdlIiwiYWRkTGlzdGVuZXIiLCJtZXNzYWdlIiwiY29uc29sZSIsImxvZyIsIk9iamVjdCIsImtleXMiLCJtZXNzYWdlSGFuZGxlcnMiLCJpbmNsdWRlcyIsInR5cGUiLCJNRVNTQUdFX1RZUEVfX1NZTkNIUk9OSVpFIiwic3luY2hyb25pemUiLCJNRVNTQUdFX1RZUEVfX1NZTkNIUk9OSVpFRCIsInN5bmNocm9uaXplZCIsInRhYklkIiwiZXZlbnRUYWJJZCIsImluZm8iLCJzdGF0dXMiLCJ0YWJzIiwiZXhlY3V0ZVNjcmlwdCIsImZpbGUiLCJob3N0bmFtZSIsImNyZWF0ZSIsInVybCIsInRhYiIsInVwZGF0ZXIiLCJjcmVhdGVVcGRhdGVyIiwiaWQiLCJvblVwZGF0ZWQiLCJzZXQiLCJNRVNTQUdFX1RZUEVfX1NFVF9PUkdBTklaQVRJT05TIl0sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtDQUlBOztBQUVBLElBQU1BLElBQUksR0FBRztBQUNYQyxlQUFhLEVBQUU7QUFESixDQUFiO0FBSUFDLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlQyxLQUFmLENBQXFCQyxHQUFyQixDQUNFLENBQUMsZUFBRCxDQURGLEVBRUU7QUFBQSxNQUFFSixhQUFGLFFBQUVBLGFBQUY7QUFBQSxTQUFxQkQsSUFBSSxDQUFDQyxhQUFMLEdBQXFCQSxhQUFhLElBQUksRUFBM0Q7QUFBQSxDQUZGO0FBS0FDLE1BQU0sQ0FBQ0ksT0FBUCxDQUFlQyxTQUFmLENBQXlCQyxXQUF6QixDQUFxQyxVQUFBQyxPQUFPLEVBQUk7QUFDOUNDLFNBQU8sQ0FBQ0MsR0FBUixDQUFZLG9CQUFaLEVBQWtDRixPQUFsQztBQUVBLE1BQUksQ0FBQ0csTUFBTSxDQUFDQyxJQUFQLENBQVlDLHdEQUFaLEVBQTZCQyxRQUE3QixDQUFzQ04sT0FBTyxDQUFDTyxJQUE5QyxDQUFMLEVBQTBEO0FBRTFERiwwREFBZSxDQUFDTCxPQUFPLENBQUNPLElBQVQsQ0FBZixDQUE4QmhCLElBQTlCLEVBQW9DUyxPQUFwQztBQUVBQyxTQUFPLENBQUNDLEdBQVIsQ0FBWSxzQkFBWixFQUFvQ1gsSUFBcEM7QUFDRCxDQVJELEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZkE7QUFFQTtBQUNBO0NBT0E7O0FBRUEsa0lBQ0dpQix1RUFESCxFQUNnQ0Msb0RBRGhDLDBDQUVHQyx3RUFGSCxFQUVnQ0MscURBRmhDLDBCOzs7Ozs7Ozs7Ozs7QUNaQTtBQUFBO0FBRWUseUVBQUFDLEtBQUs7QUFBQSxTQUFJLFVBQUNDLFVBQUQsRUFBYUMsSUFBYixFQUFzQjtBQUM1QyxRQUFJRixLQUFLLElBQUlDLFVBQWIsRUFBeUI7QUFFekIsUUFBSUMsSUFBSSxDQUFDQyxNQUFMLElBQWUsVUFBbkIsRUFBK0I7QUFFL0J0QixVQUFNLENBQUN1QixJQUFQLENBQVlDLGFBQVosQ0FBMEJMLEtBQTFCLEVBQWlDO0FBQUNNLFVBQUksRUFBRTtBQUFQLEtBQWpDO0FBQ0QsR0FObUI7QUFBQSxDQUFwQixFOzs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7Q0FJQTs7QUFFZSx5RUFBQzNCLElBQUQsUUFBc0I7QUFBQSxNQUFkNEIsUUFBYyxRQUFkQSxRQUFjO0FBQ25DMUIsUUFBTSxDQUFDdUIsSUFBUCxDQUFZSSxNQUFaLENBQ0U7QUFDRUMsT0FBRyxZQUFLRixRQUFMO0FBREwsR0FERixFQUlFLFVBQUFHLEdBQUcsRUFBSTtBQUNMLFFBQU1DLE9BQU8sR0FBR0MsOERBQWEsQ0FBQ0YsR0FBRyxDQUFDRyxFQUFMLENBQTdCO0FBRUFoQyxVQUFNLENBQUN1QixJQUFQLENBQVlVLFNBQVosQ0FBc0IzQixXQUF0QixDQUFrQ3dCLE9BQWxDO0FBQ0QsR0FSSDtBQVVELENBWEQsRTs7Ozs7Ozs7Ozs7O0FDTkE7QUFBQTtBQUVlLHlFQUFDaEMsSUFBRCxRQUEyQjtBQUFBLE1BQW5CQyxhQUFtQixRQUFuQkEsYUFBbUI7QUFDeENELE1BQUksQ0FBQ0MsYUFBTCxHQUFxQkEsYUFBckI7QUFFQUMsUUFBTSxDQUFDQyxPQUFQLENBQWVDLEtBQWYsQ0FBcUJnQyxHQUFyQixDQUNFO0FBQUNuQyxpQkFBYSxFQUFiQTtBQUFELEdBREYsRUFFRTtBQUFBLFdBQU1TLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLHFCQUFaLENBQU47QUFBQSxHQUZGO0FBSUQsQ0FQRCxFOzs7Ozs7Ozs7Ozs7QUNGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUEsSUFBTTBCLCtCQUErQixHQUFHLDhCQUF4QztBQUFBLElBQ01wQix5QkFBeUIsR0FBUyx5QkFEeEM7QUFBQSxJQUVNRSwwQkFBMEIsR0FBUSwwQkFGeEMiLCJmaWxlIjoiYmFja2dyb3VuZC5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL2JhY2tncm91bmQvaW5kZXguanNcIik7XG4iLCIvLyBJbnRlcm5hbCBEZXBlbmRlbmNpZXNcblxuaW1wb3J0IG1lc3NhZ2VIYW5kbGVycyBmcm9tICcuL21lc3NhZ2VIYW5kbGVycydcblxuLy8gTW9kdWxlXG5cbmNvbnN0IGRhdGEgPSB7XG4gIG9yZ2FuaXphdGlvbnM6IFtdLFxufVxuXG5jaHJvbWUuc3RvcmFnZS5sb2NhbC5nZXQoXG4gIFsnb3JnYW5pemF0aW9ucyddLFxuICAoe29yZ2FuaXphdGlvbnN9KSA9PiBkYXRhLm9yZ2FuaXphdGlvbnMgPSBvcmdhbml6YXRpb25zIHx8IFtdLFxuKVxuXG5jaHJvbWUucnVudGltZS5vbk1lc3NhZ2UuYWRkTGlzdGVuZXIobWVzc2FnZSA9PiB7XG4gIGNvbnNvbGUubG9nKCdiYWNrZ3JvdW5kIG1lc3NhZ2UnLCBtZXNzYWdlKVxuXG4gIGlmICghT2JqZWN0LmtleXMobWVzc2FnZUhhbmRsZXJzKS5pbmNsdWRlcyhtZXNzYWdlLnR5cGUpKSByZXR1cm5cblxuICBtZXNzYWdlSGFuZGxlcnNbbWVzc2FnZS50eXBlXShkYXRhLCBtZXNzYWdlKVxuXG4gIGNvbnNvbGUubG9nKCduZXcgYmFja2dyb3VuZCBzdGF0ZScsIGRhdGEpXG59KVxuIiwiLy8gSW50ZXJuYWwgRGVwZW5kZW5jaWVzXG5cbmltcG9ydCBzeW5jaHJvbml6ZSAgZnJvbSAnLi9zeW5jaHJvbml6ZSdcbmltcG9ydCBzeW5jaHJvbml6ZWQgZnJvbSAnLi9zeW5jaHJvbml6ZWQnXG5cbmltcG9ydCB7XG4gIE1FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkUsXG4gIE1FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkVELFxufSBmcm9tICdAL21lc3NhZ2VUeXBlcydcblxuLy8gTW9kdWxlXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgW01FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkVdOiAgc3luY2hyb25pemUsXG4gIFtNRVNTQUdFX1RZUEVfX1NZTkNIUk9OSVpFRF06IHN5bmNocm9uaXplZCxcbn1cbiIsIi8vIE1vZHVsZVxuXG5leHBvcnQgZGVmYXVsdCB0YWJJZCA9PiAoZXZlbnRUYWJJZCwgaW5mbykgPT4ge1xuICBpZiAodGFiSWQgIT0gZXZlbnRUYWJJZCkgcmV0dXJuXG5cbiAgaWYgKGluZm8uc3RhdHVzICE9ICdjb21wbGV0ZScpIHJldHVyblxuXG4gIGNocm9tZS50YWJzLmV4ZWN1dGVTY3JpcHQodGFiSWQsIHtmaWxlOiAnc3luY2hyb25pemVPcmNoZXN0cmF0aW9uLmpzJ30pXG59XG4iLCIvLyBJbnRlcm5hbCBEZXBlbmRlbmNpZXNcblxuaW1wb3J0IGNyZWF0ZVVwZGF0ZXIgZnJvbSAnLi9jcmVhdGVVcGRhdGVyJ1xuXG4vLyBNb2R1bGVcblxuZXhwb3J0IGRlZmF1bHQgKGRhdGEsIHtob3N0bmFtZX0pID0+IHtcbiAgY2hyb21lLnRhYnMuY3JlYXRlKFxuICAgIHtcbiAgICAgIHVybDogYCR7aG9zdG5hbWV9cGxhbnNgLFxuICAgIH0sXG4gICAgdGFiID0+IHtcbiAgICAgIGNvbnN0IHVwZGF0ZXIgPSBjcmVhdGVVcGRhdGVyKHRhYi5pZClcblxuICAgICAgY2hyb21lLnRhYnMub25VcGRhdGVkLmFkZExpc3RlbmVyKHVwZGF0ZXIpXG4gICAgfVxuICApXG59XG4iLCIvLyBNb2R1bGVcblxuZXhwb3J0IGRlZmF1bHQgKGRhdGEsIHtvcmdhbml6YXRpb25zfSkgPT4ge1xuICBkYXRhLm9yZ2FuaXphdGlvbnMgPSBvcmdhbml6YXRpb25zXG5cbiAgY2hyb21lLnN0b3JhZ2UubG9jYWwuc2V0KFxuICAgIHtvcmdhbml6YXRpb25zfSxcbiAgICAoKSA9PiBjb25zb2xlLmxvZygnb3JnYW5pemF0aW9ucyBzYXZlZCcpXG4gIClcbn1cbiIsIi8vIE1vZHVsZVxuXG5jb25zdCBNRVNTQUdFX1RZUEVfX1NFVF9PUkdBTklaQVRJT05TID0gJ21lc3NhZ2VUeXBlOnNldE9yZ2FuaXphdGlvbnMnXG4gICAgLCBNRVNTQUdFX1RZUEVfX1NZTkNIUk9OSVpFICAgICAgID0gJ21lc3NhZ2VUeXBlOnN5bmNocm9uaXplJ1xuICAgICwgTUVTU0FHRV9UWVBFX19TWU5DSFJPTklaRUQgICAgICA9ICdtZXNzYWdlVHlwZTpzeW5jaHJvbml6ZWQnXG5cbmV4cG9ydCB7XG4gIE1FU1NBR0VfVFlQRV9fU0VUX09SR0FOSVpBVElPTlMsXG4gIE1FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkUsXG4gIE1FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkVELFxufVxuIl0sInNvdXJjZVJvb3QiOiIifQ==