/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/synchronizeOrchestration/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/messageTypes.js":
/*!*****************************!*\
  !*** ./src/messageTypes.js ***!
  \*****************************/
/*! exports provided: MESSAGE_TYPE__SET_ORGANIZATIONS, MESSAGE_TYPE__SYNCHRONIZE, MESSAGE_TYPE__SYNCHRONIZED */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MESSAGE_TYPE__SET_ORGANIZATIONS", function() { return MESSAGE_TYPE__SET_ORGANIZATIONS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MESSAGE_TYPE__SYNCHRONIZE", function() { return MESSAGE_TYPE__SYNCHRONIZE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MESSAGE_TYPE__SYNCHRONIZED", function() { return MESSAGE_TYPE__SYNCHRONIZED; });
// Module
var MESSAGE_TYPE__SET_ORGANIZATIONS = 'messageType:setOrganizations',
    MESSAGE_TYPE__SYNCHRONIZE = 'messageType:synchronize',
    MESSAGE_TYPE__SYNCHRONIZED = 'messageType:synchronized';


/***/ }),

/***/ "./src/messages/index.js":
/*!*******************************!*\
  !*** ./src/messages/index.js ***!
  \*******************************/
/*! exports provided: synchronize, synchronized */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _synchronize__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./synchronize */ "./src/messages/synchronize.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "synchronize", function() { return _synchronize__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _synchronized__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./synchronized */ "./src/messages/synchronized.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "synchronized", function() { return _synchronized__WEBPACK_IMPORTED_MODULE_1__["default"]; });

// Internal Dependencies

 // Module



/***/ }),

/***/ "./src/messages/synchronize.js":
/*!*************************************!*\
  !*** ./src/messages/synchronize.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _messageTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/messageTypes */ "./src/messageTypes.js");
// Internal Dependencies
 // Module

/* harmony default export */ __webpack_exports__["default"] = (function (hostname) {
  return chrome.runtime.sendMessage({
    type: _messageTypes__WEBPACK_IMPORTED_MODULE_0__["MESSAGE_TYPE__SYNCHRONIZE"],
    hostname: hostname
  });
});

/***/ }),

/***/ "./src/messages/synchronized.js":
/*!**************************************!*\
  !*** ./src/messages/synchronized.js ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _messageTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/messageTypes */ "./src/messageTypes.js");
// Internal Dependencies
 // Module

/* harmony default export */ __webpack_exports__["default"] = (function (organizations) {
  return chrome.runtime.sendMessage({
    type: _messageTypes__WEBPACK_IMPORTED_MODULE_0__["MESSAGE_TYPE__SYNCHRONIZED"],
    organizations: organizations
  });
});

/***/ }),

/***/ "./src/synchronizeOrchestration/createOverlay/createStyleAttribute.js":
/*!****************************************************************************!*\
  !*** ./src/synchronizeOrchestration/createOverlay/createStyleAttribute.js ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

// Module
/* harmony default export */ __webpack_exports__["default"] = (function (style) {
  return Object.entries(style).map(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        key = _ref2[0],
        value = _ref2[1];

    return "".concat(key, ": ").concat(value);
  }).join('; ');
});

/***/ }),

/***/ "./src/synchronizeOrchestration/createOverlay/index.js":
/*!*************************************************************!*\
  !*** ./src/synchronizeOrchestration/createOverlay/index.js ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _createStyleAttribute__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./createStyleAttribute */ "./src/synchronizeOrchestration/createOverlay/createStyleAttribute.js");
// Internal Dependencies
 // Module

/* harmony default export */ __webpack_exports__["default"] = (function (data) {
  var overlay = document.createElement('div'),
      overlayText = document.createElement('div'),
      overlayStyle = {
    'position': 'absolute',
    'left': 0,
    'right': 0,
    'top': 0,
    'bottom': 0,
    'z-index': 10000000000,
    'backdrop-filter': 'blur(3px)',
    'background': 'rgba(255, 255, 255, 0.381966)',
    'color': '#000',
    'font-size': '36pt',
    'text-shadow': '0 0 5px #fff'
  },
      overlayTextStyle = {
    'position': 'absolute',
    'top': '50%',
    'left': '10%',
    'right': '10%',
    'transform': 'translate(0, -50%)',
    'text-align': 'center'
  },
      overlayStyleAttribute = Object(_createStyleAttribute__WEBPACK_IMPORTED_MODULE_0__["default"])(overlayStyle),
      overlayTextStyleAttribute = Object(_createStyleAttribute__WEBPACK_IMPORTED_MODULE_0__["default"])(overlayTextStyle);
  overlayText.textContent = 'Please wait';
  overlayText.setAttribute('style', overlayTextStyleAttribute);
  overlay.setAttribute('style', overlayStyleAttribute);
  overlay.appendChild(overlayText);
  document.body.appendChild(overlay);
  data.overlay = {
    updateText: function updateText(text) {
      return overlayText.textContent = text;
    }
  };
  return Promise.resolve();
});

/***/ }),

/***/ "./src/synchronizeOrchestration/fetchOrganizations.js":
/*!************************************************************!*\
  !*** ./src/synchronizeOrchestration/fetchOrganizations.js ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// Module
/* harmony default export */ __webpack_exports__["default"] = (function (data) {
  return new Promise(function (resolve, reject) {
    var options = {
      body: null,
      cache: 'reload',
      credentials: 'include',
      method: 'GET',
      mode: 'cors',
      referrer: "".concat(data.hostname, "plans"),
      referrerPolicy: 'no-referrer-when-downgrade',
      headers: {
        'accept': 'application/json',
        'accept-language': 'en-US,en;q=0.9',
        'if-none-match': 'W/\"195d-tF3AVWbJ6IzGAThIWuEu6cZpHU8\"',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin'
      }
    },
        url = "".concat(data.hostname, "manager/api/public/organizations/permissions") + "?action=read&resource=plan";
    console.log({
      url: url
    });
    data.overlay.updateText('Fetching organizations...');
    fetch(url, options).then(function (response) {
      return response.json();
    }).then(function (organizations) {
      data.organizations = organizations.reduce(function (organizationMap, organization) {
        organizationMap[organization.id] = organization;
        return organizationMap;
      }, {});
      resolve();
    })["catch"](function (error) {
      return console.log('fetchOrganizations error', error);
    });
  });
});

/***/ }),

/***/ "./src/synchronizeOrchestration/fetchPlans/fetchPlan.js":
/*!**************************************************************!*\
  !*** ./src/synchronizeOrchestration/fetchPlans/fetchPlan.js ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// Module
var fetchPlan = function fetchPlan(data, organization, remaining) {
  return new Promise(function (resolve, reject) {
    var options = {
      body: null,
      credentials: 'include',
      method: 'GET',
      mode: 'cors',
      referrer: "".concat(data.hostname, "plans/").concat(organization.id, "/list"),
      referrerPolicy: 'no-referrer-when-downgrade',
      headers: {
        'accept': 'application/json',
        'accept-language': 'en-US,en;q=0.9',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin'
      }
    },
        url = "".concat(data.hostname, "manager/api/plans") + "?organizationId=".concat(organization.id);
    data.overlay.updateText("Fetching plans for ".concat(organization.name, "..."));
    fetch(url, options).then(function (response) {
      return response.json();
    }).then(function (plans) {
      organization.plans = plans.reduce(function (planMap, plan) {
        planMap[plan.id] = plan;
        return planMap;
      }, {});

      if (remaining.length) {
        var nextOrganization = remaining.shift();
        fetchPlan(data, nextOrganization, remaining).then(resolve);
      } else {
        resolve();
      }
    })["catch"](function (error) {
      return console.log('fetchPlan error', error);
    });
  });
};

/* harmony default export */ __webpack_exports__["default"] = (fetchPlan);

/***/ }),

/***/ "./src/synchronizeOrchestration/fetchPlans/index.js":
/*!**********************************************************!*\
  !*** ./src/synchronizeOrchestration/fetchPlans/index.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _fetchPlan__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fetchPlan */ "./src/synchronizeOrchestration/fetchPlans/fetchPlan.js");
// Internal Dependencies
 // Module

/* harmony default export */ __webpack_exports__["default"] = (function (data) {
  return new Promise(function (resolve, reject) {
    var remaining = Object.values(data.organizations),
        organization = remaining.shift();
    Object(_fetchPlan__WEBPACK_IMPORTED_MODULE_0__["default"])(data, organization, remaining).then(resolve);
  });
});

/***/ }),

/***/ "./src/synchronizeOrchestration/getHostname.js":
/*!*****************************************************!*\
  !*** ./src/synchronizeOrchestration/getHostname.js ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// Module
/* harmony default export */ __webpack_exports__["default"] = (function (data) {
  return new Promise(function (resolve, reject) {
    var url = new URL(window.location.href);
    url.pathname = '';
    data.hostname = url.toString();
    resolve();
  });
});

/***/ }),

/***/ "./src/synchronizeOrchestration/index.js":
/*!***********************************************!*\
  !*** ./src/synchronizeOrchestration/index.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _createOverlay__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./createOverlay */ "./src/synchronizeOrchestration/createOverlay/index.js");
/* harmony import */ var _getHostname__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./getHostname */ "./src/synchronizeOrchestration/getHostname.js");
/* harmony import */ var _fetchOrganizations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fetchOrganizations */ "./src/synchronizeOrchestration/fetchOrganizations.js");
/* harmony import */ var _fetchPlans__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./fetchPlans */ "./src/synchronizeOrchestration/fetchPlans/index.js");
/* harmony import */ var _messages__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/messages */ "./src/messages/index.js");
// Internal Dependencies




 // Module

var data = {
  hostname: '',
  organizations: []
};
Object(_createOverlay__WEBPACK_IMPORTED_MODULE_0__["default"])(data).then(function () {
  return Object(_getHostname__WEBPACK_IMPORTED_MODULE_1__["default"])(data);
}).then(function () {
  return Object(_fetchOrganizations__WEBPACK_IMPORTED_MODULE_2__["default"])(data);
}).then(function () {
  return Object(_fetchPlans__WEBPACK_IMPORTED_MODULE_3__["default"])(data);
}).then(function () {
  console.log('All plan data fetched', data);
  data.overlay.updateText('Complete');

  Object(_messages__WEBPACK_IMPORTED_MODULE_4__["synchronized"])(data.organizations);

  window.close();
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL21lc3NhZ2VUeXBlcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbWVzc2FnZXMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL21lc3NhZ2VzL3N5bmNocm9uaXplLmpzIiwid2VicGFjazovLy8uL3NyYy9tZXNzYWdlcy9zeW5jaHJvbml6ZWQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3N5bmNocm9uaXplT3JjaGVzdHJhdGlvbi9jcmVhdGVPdmVybGF5L2NyZWF0ZVN0eWxlQXR0cmlidXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9zeW5jaHJvbml6ZU9yY2hlc3RyYXRpb24vY3JlYXRlT3ZlcmxheS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc3luY2hyb25pemVPcmNoZXN0cmF0aW9uL2ZldGNoT3JnYW5pemF0aW9ucy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc3luY2hyb25pemVPcmNoZXN0cmF0aW9uL2ZldGNoUGxhbnMvZmV0Y2hQbGFuLmpzIiwid2VicGFjazovLy8uL3NyYy9zeW5jaHJvbml6ZU9yY2hlc3RyYXRpb24vZmV0Y2hQbGFucy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc3luY2hyb25pemVPcmNoZXN0cmF0aW9uL2dldEhvc3RuYW1lLmpzIiwid2VicGFjazovLy8uL3NyYy9zeW5jaHJvbml6ZU9yY2hlc3RyYXRpb24vaW5kZXguanMiXSwibmFtZXMiOlsiTUVTU0FHRV9UWVBFX19TRVRfT1JHQU5JWkFUSU9OUyIsIk1FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkUiLCJNRVNTQUdFX1RZUEVfX1NZTkNIUk9OSVpFRCIsImhvc3RuYW1lIiwiY2hyb21lIiwicnVudGltZSIsInNlbmRNZXNzYWdlIiwidHlwZSIsIm9yZ2FuaXphdGlvbnMiLCJzdHlsZSIsIk9iamVjdCIsImVudHJpZXMiLCJtYXAiLCJrZXkiLCJ2YWx1ZSIsImpvaW4iLCJkYXRhIiwib3ZlcmxheSIsImRvY3VtZW50IiwiY3JlYXRlRWxlbWVudCIsIm92ZXJsYXlUZXh0Iiwib3ZlcmxheVN0eWxlIiwib3ZlcmxheVRleHRTdHlsZSIsIm92ZXJsYXlTdHlsZUF0dHJpYnV0ZSIsImNyZWF0ZVN0eWxlQXR0cmlidXRlIiwib3ZlcmxheVRleHRTdHlsZUF0dHJpYnV0ZSIsInRleHRDb250ZW50Iiwic2V0QXR0cmlidXRlIiwiYXBwZW5kQ2hpbGQiLCJib2R5IiwidXBkYXRlVGV4dCIsInRleHQiLCJQcm9taXNlIiwicmVzb2x2ZSIsInJlamVjdCIsIm9wdGlvbnMiLCJjYWNoZSIsImNyZWRlbnRpYWxzIiwibWV0aG9kIiwibW9kZSIsInJlZmVycmVyIiwicmVmZXJyZXJQb2xpY3kiLCJoZWFkZXJzIiwidXJsIiwiY29uc29sZSIsImxvZyIsImZldGNoIiwidGhlbiIsInJlc3BvbnNlIiwianNvbiIsInJlZHVjZSIsIm9yZ2FuaXphdGlvbk1hcCIsIm9yZ2FuaXphdGlvbiIsImlkIiwiZXJyb3IiLCJmZXRjaFBsYW4iLCJyZW1haW5pbmciLCJuYW1lIiwicGxhbnMiLCJwbGFuTWFwIiwicGxhbiIsImxlbmd0aCIsIm5leHRPcmdhbml6YXRpb24iLCJzaGlmdCIsInZhbHVlcyIsIlVSTCIsIndpbmRvdyIsImxvY2F0aW9uIiwiaHJlZiIsInBhdGhuYW1lIiwidG9TdHJpbmciLCJjcmVhdGVPdmVybGF5IiwiZ2V0SG9zdG5hbWUiLCJmZXRjaE9yZ2FuaXphdGlvbnMiLCJmZXRjaFBsYW5zIiwic3luY2hyb25pemVkIiwiY2xvc2UiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVBLElBQU1BLCtCQUErQixHQUFHLDhCQUF4QztBQUFBLElBQ01DLHlCQUF5QixHQUFTLHlCQUR4QztBQUFBLElBRU1DLDBCQUEwQixHQUFRLDBCQUZ4Qzs7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFQTtDQUdBOzs7Ozs7Ozs7Ozs7OztBQ0xBO0FBQUE7QUFBQTtDQUlBOztBQUVlLHlFQUFBQyxRQUFRO0FBQUEsU0FDckJDLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlQyxXQUFmLENBQTJCO0FBQ3pCQyxRQUFJLEVBQUVOLHVFQURtQjtBQUV6QkUsWUFBUSxFQUFSQTtBQUZ5QixHQUEzQixDQURxQjtBQUFBLENBQXZCLEU7Ozs7Ozs7Ozs7OztBQ05BO0FBQUE7QUFBQTtDQUlBOztBQUVlLHlFQUFBSyxhQUFhO0FBQUEsU0FDMUJKLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlQyxXQUFmLENBQTJCO0FBQ3pCQyxRQUFJLEVBQUVMLHdFQURtQjtBQUV6Qk0saUJBQWEsRUFBYkE7QUFGeUIsR0FBM0IsQ0FEMEI7QUFBQSxDQUE1QixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNOQTtBQUVlLHlFQUFBQyxLQUFLO0FBQUEsU0FBSUMsTUFBTSxDQUNIQyxPQURILENBQ1dGLEtBRFgsRUFFR0csR0FGSCxDQUVPO0FBQUE7QUFBQSxRQUFFQyxHQUFGO0FBQUEsUUFBT0MsS0FBUDs7QUFBQSxxQkFBcUJELEdBQXJCLGVBQTZCQyxLQUE3QjtBQUFBLEdBRlAsRUFHR0MsSUFISCxDQUdRLElBSFIsQ0FBSjtBQUFBLENBQXBCLEU7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtDQUlBOztBQUVlLHlFQUFBQyxJQUFJLEVBQUk7QUFDckIsTUFBTUMsT0FBTyxHQUFZQyxRQUFRLENBQUNDLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBekI7QUFBQSxNQUNNQyxXQUFXLEdBQVFGLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixLQUF2QixDQUR6QjtBQUFBLE1BRU1FLFlBQVksR0FBTztBQUNFLGdCQUFhLFVBRGY7QUFFRSxZQUFhLENBRmY7QUFHRSxhQUFhLENBSGY7QUFJRSxXQUFhLENBSmY7QUFLRSxjQUFhLENBTGY7QUFNRSxlQUFhLFdBTmY7QUFRRSx1QkFBbUIsV0FSckI7QUFTRSxrQkFBbUIsK0JBVHJCO0FBVUUsYUFBbUIsTUFWckI7QUFXRSxpQkFBbUIsTUFYckI7QUFZRSxtQkFBbUI7QUFackIsR0FGekI7QUFBQSxNQWdCTUMsZ0JBQWdCLEdBQUc7QUFDRSxnQkFBYSxVQURmO0FBRUUsV0FBYSxLQUZmO0FBR0UsWUFBYSxLQUhmO0FBSUUsYUFBYSxLQUpmO0FBS0UsaUJBQWEsb0JBTGY7QUFPRSxrQkFBYztBQVBoQixHQWhCekI7QUFBQSxNQTBCTUMscUJBQXFCLEdBQU9DLHFFQUFvQixDQUFDSCxZQUFELENBMUJ0RDtBQUFBLE1BMkJNSSx5QkFBeUIsR0FBR0QscUVBQW9CLENBQUNGLGdCQUFELENBM0J0RDtBQThCQUYsYUFBVyxDQUFDTSxXQUFaLEdBQTBCLGFBQTFCO0FBQ0FOLGFBQVcsQ0FBQ08sWUFBWixDQUF5QixPQUF6QixFQUFrQ0YseUJBQWxDO0FBRUFSLFNBQU8sQ0FBQ1UsWUFBUixDQUFxQixPQUFyQixFQUE4QkoscUJBQTlCO0FBQ0FOLFNBQU8sQ0FBQ1csV0FBUixDQUFvQlIsV0FBcEI7QUFFQUYsVUFBUSxDQUFDVyxJQUFULENBQWNELFdBQWQsQ0FBMEJYLE9BQTFCO0FBRUFELE1BQUksQ0FBQ0MsT0FBTCxHQUFlO0FBQUVhLGNBQVUsRUFBRSxvQkFBQUMsSUFBSTtBQUFBLGFBQUlYLFdBQVcsQ0FBQ00sV0FBWixHQUEwQkssSUFBOUI7QUFBQTtBQUFsQixHQUFmO0FBRUEsU0FBT0MsT0FBTyxDQUFDQyxPQUFSLEVBQVA7QUFDRCxDQTFDRCxFOzs7Ozs7Ozs7Ozs7QUNOQTtBQUFBO0FBRWUseUVBQUFqQixJQUFJO0FBQUEsU0FBSSxJQUFJZ0IsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUN0RCxRQUFNQyxPQUFPLEdBQUc7QUFDRU4sVUFBSSxFQUFZLElBRGxCO0FBRUVPLFdBQUssRUFBVyxRQUZsQjtBQUdFQyxpQkFBVyxFQUFLLFNBSGxCO0FBSUVDLFlBQU0sRUFBVSxLQUpsQjtBQUtFQyxVQUFJLEVBQVksTUFMbEI7QUFNRUMsY0FBUSxZQUFXeEIsSUFBSSxDQUFDYixRQUFoQixVQU5WO0FBT0VzQyxvQkFBYyxFQUFFLDRCQVBsQjtBQVFFQyxhQUFPLEVBQUU7QUFDUCxrQkFBbUIsa0JBRFo7QUFFUCwyQkFBbUIsZ0JBRlo7QUFHUCx5QkFBbUIsd0NBSFo7QUFJUCwwQkFBbUIsTUFKWjtBQUtQLDBCQUFtQjtBQUxaO0FBUlgsS0FBaEI7QUFBQSxRQWdCTUMsR0FBRyxHQUFPLFVBQUczQixJQUFJLENBQUNiLFFBQVIsZ0ZBaEJoQjtBQW1CQXlDLFdBQU8sQ0FBQ0MsR0FBUixDQUFZO0FBQUNGLFNBQUcsRUFBSEE7QUFBRCxLQUFaO0FBRUEzQixRQUFJLENBQUNDLE9BQUwsQ0FBYWEsVUFBYixDQUF3QiwyQkFBeEI7QUFFQWdCLFNBQUssQ0FBQ0gsR0FBRCxFQUFNUixPQUFOLENBQUwsQ0FDR1ksSUFESCxDQUNRLFVBQUFDLFFBQVE7QUFBQSxhQUFJQSxRQUFRLENBQUNDLElBQVQsRUFBSjtBQUFBLEtBRGhCLEVBRUdGLElBRkgsQ0FFUSxVQUFBdkMsYUFBYSxFQUFJO0FBQ3JCUSxVQUFJLENBQUNSLGFBQUwsR0FBcUJBLGFBQWEsQ0FBQzBDLE1BQWQsQ0FDbkIsVUFBQ0MsZUFBRCxFQUFrQkMsWUFBbEIsRUFBbUM7QUFDakNELHVCQUFlLENBQUNDLFlBQVksQ0FBQ0MsRUFBZCxDQUFmLEdBQW1DRCxZQUFuQztBQUVBLGVBQU9ELGVBQVA7QUFDRCxPQUxrQixFQU1uQixFQU5tQixDQUFyQjtBQVNBbEIsYUFBTztBQUNSLEtBYkgsV0FjUyxVQUFBcUIsS0FBSztBQUFBLGFBQUlWLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLDBCQUFaLEVBQXdDUyxLQUF4QyxDQUFKO0FBQUEsS0FkZDtBQWVELEdBdkNzQixDQUFKO0FBQUEsQ0FBbkIsRTs7Ozs7Ozs7Ozs7O0FDRkE7QUFBQTtBQUVBLElBQU1DLFNBQVMsR0FBRyxTQUFaQSxTQUFZLENBQUN2QyxJQUFELEVBQU9vQyxZQUFQLEVBQXFCSSxTQUFyQjtBQUFBLFNBQW1DLElBQUl4QixPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3BGLFFBQU1DLE9BQU8sR0FBRztBQUNFTixVQUFJLEVBQVksSUFEbEI7QUFFRVEsaUJBQVcsRUFBSyxTQUZsQjtBQUdFQyxZQUFNLEVBQVUsS0FIbEI7QUFJRUMsVUFBSSxFQUFZLE1BSmxCO0FBS0VDLGNBQVEsWUFBV3hCLElBQUksQ0FBQ2IsUUFBaEIsbUJBQWlDaUQsWUFBWSxDQUFDQyxFQUE5QyxVQUxWO0FBTUVaLG9CQUFjLEVBQUUsNEJBTmxCO0FBT0VDLGFBQU8sRUFBRTtBQUNQLGtCQUFtQixrQkFEWjtBQUVQLDJCQUFtQixnQkFGWjtBQUdQLDBCQUFtQixNQUhaO0FBSVAsMEJBQW1CO0FBSlo7QUFQWCxLQUFoQjtBQUFBLFFBY01DLEdBQUcsR0FBTyxVQUFHM0IsSUFBSSxDQUFDYixRQUFSLG1EQUNtQmlELFlBQVksQ0FBQ0MsRUFEaEMsQ0FkaEI7QUFpQkFyQyxRQUFJLENBQUNDLE9BQUwsQ0FBYWEsVUFBYiw4QkFBOENzQixZQUFZLENBQUNLLElBQTNEO0FBRUFYLFNBQUssQ0FBQ0gsR0FBRCxFQUFNUixPQUFOLENBQUwsQ0FDR1ksSUFESCxDQUNRLFVBQUFDLFFBQVE7QUFBQSxhQUFJQSxRQUFRLENBQUNDLElBQVQsRUFBSjtBQUFBLEtBRGhCLEVBRUdGLElBRkgsQ0FFUSxVQUFBVyxLQUFLLEVBQUk7QUFDYk4sa0JBQVksQ0FBQ00sS0FBYixHQUFxQkEsS0FBSyxDQUFDUixNQUFOLENBQ25CLFVBQUNTLE9BQUQsRUFBVUMsSUFBVixFQUFtQjtBQUNqQkQsZUFBTyxDQUFDQyxJQUFJLENBQUNQLEVBQU4sQ0FBUCxHQUFtQk8sSUFBbkI7QUFFQSxlQUFPRCxPQUFQO0FBQ0QsT0FMa0IsRUFNbkIsRUFObUIsQ0FBckI7O0FBU0EsVUFBSUgsU0FBUyxDQUFDSyxNQUFkLEVBQXNCO0FBQ3BCLFlBQU1DLGdCQUFnQixHQUFHTixTQUFTLENBQUNPLEtBQVYsRUFBekI7QUFFQVIsaUJBQVMsQ0FBQ3ZDLElBQUQsRUFBTzhDLGdCQUFQLEVBQXlCTixTQUF6QixDQUFULENBQ0dULElBREgsQ0FDUWQsT0FEUjtBQUVELE9BTEQsTUFLTztBQUNMQSxlQUFPO0FBQ1I7QUFDRixLQXBCSCxXQXFCTyxVQUFBcUIsS0FBSztBQUFBLGFBQUlWLE9BQU8sQ0FBQ0MsR0FBUixDQUFZLGlCQUFaLEVBQStCUyxLQUEvQixDQUFKO0FBQUEsS0FyQlo7QUFzQkQsR0ExQ29ELENBQW5DO0FBQUEsQ0FBbEI7O0FBNENlQyx3RUFBZixFOzs7Ozs7Ozs7Ozs7QUM5Q0E7QUFBQTtBQUFBO0NBSUE7O0FBRWUseUVBQUF2QyxJQUFJO0FBQUEsU0FBSSxJQUFJZ0IsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBVUMsTUFBVixFQUFxQjtBQUN0RCxRQUFNc0IsU0FBUyxHQUFHOUMsTUFBTSxDQUFDc0QsTUFBUCxDQUFjaEQsSUFBSSxDQUFDUixhQUFuQixDQUFsQjtBQUFBLFFBRU00QyxZQUFZLEdBQUdJLFNBQVMsQ0FBQ08sS0FBVixFQUZyQjtBQUlBUiw4REFBUyxDQUFDdkMsSUFBRCxFQUFPb0MsWUFBUCxFQUFxQkksU0FBckIsQ0FBVCxDQUF5Q1QsSUFBekMsQ0FBOENkLE9BQTlDO0FBQ0QsR0FOc0IsQ0FBSjtBQUFBLENBQW5CLEU7Ozs7Ozs7Ozs7OztBQ05BO0FBQUE7QUFFZSx5RUFBQWpCLElBQUk7QUFBQSxTQUFJLElBQUlnQixPQUFKLENBQVksVUFBQ0MsT0FBRCxFQUFVQyxNQUFWLEVBQXFCO0FBQ3RELFFBQU1TLEdBQUcsR0FBRyxJQUFJc0IsR0FBSixDQUFRQyxNQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLElBQXhCLENBQVo7QUFFQXpCLE9BQUcsQ0FBQzBCLFFBQUosR0FBZSxFQUFmO0FBRUFyRCxRQUFJLENBQUNiLFFBQUwsR0FBZ0J3QyxHQUFHLENBQUMyQixRQUFKLEVBQWhCO0FBRUFyQyxXQUFPO0FBQ1IsR0FSc0IsQ0FBSjtBQUFBLENBQW5CLEU7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7Q0FJQTs7QUFFQSxJQUFNakIsSUFBSSxHQUFHO0FBQ1hiLFVBQVEsRUFBTyxFQURKO0FBRVhLLGVBQWEsRUFBRTtBQUZKLENBQWI7QUFLQStELDhEQUFhLENBQUN2RCxJQUFELENBQWIsQ0FDRytCLElBREgsQ0FDUTtBQUFBLFNBQU15Qiw0REFBVyxDQUFDeEQsSUFBRCxDQUFqQjtBQUFBLENBRFIsRUFFRytCLElBRkgsQ0FFUTtBQUFBLFNBQU0wQixtRUFBa0IsQ0FBQ3pELElBQUQsQ0FBeEI7QUFBQSxDQUZSLEVBR0crQixJQUhILENBR1E7QUFBQSxTQUFNMkIsMkRBQVUsQ0FBQzFELElBQUQsQ0FBaEI7QUFBQSxDQUhSLEVBSUcrQixJQUpILENBSVEsWUFBTTtBQUNWSCxTQUFPLENBQUNDLEdBQVIsQ0FBWSx1QkFBWixFQUFxQzdCLElBQXJDO0FBRUFBLE1BQUksQ0FBQ0MsT0FBTCxDQUFhYSxVQUFiLENBQXdCLFVBQXhCOztBQUVBNkMsZ0VBQVksQ0FBQzNELElBQUksQ0FBQ1IsYUFBTixDQUFaOztBQUVBMEQsUUFBTSxDQUFDVSxLQUFQO0FBQ0QsQ0FaSCxFIiwiZmlsZSI6InN5bmNocm9uaXplT3JjaGVzdHJhdGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc3JjL3N5bmNocm9uaXplT3JjaGVzdHJhdGlvbi9pbmRleC5qc1wiKTtcbiIsIi8vIE1vZHVsZVxuXG5jb25zdCBNRVNTQUdFX1RZUEVfX1NFVF9PUkdBTklaQVRJT05TID0gJ21lc3NhZ2VUeXBlOnNldE9yZ2FuaXphdGlvbnMnXG4gICAgLCBNRVNTQUdFX1RZUEVfX1NZTkNIUk9OSVpFICAgICAgID0gJ21lc3NhZ2VUeXBlOnN5bmNocm9uaXplJ1xuICAgICwgTUVTU0FHRV9UWVBFX19TWU5DSFJPTklaRUQgICAgICA9ICdtZXNzYWdlVHlwZTpzeW5jaHJvbml6ZWQnXG5cbmV4cG9ydCB7XG4gIE1FU1NBR0VfVFlQRV9fU0VUX09SR0FOSVpBVElPTlMsXG4gIE1FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkUsXG4gIE1FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkVELFxufVxuIiwiLy8gSW50ZXJuYWwgRGVwZW5kZW5jaWVzXG5cbmltcG9ydCBzeW5jaHJvbml6ZSAgZnJvbSAnLi9zeW5jaHJvbml6ZSdcbmltcG9ydCBzeW5jaHJvbml6ZWQgZnJvbSAnLi9zeW5jaHJvbml6ZWQnXG5cbi8vIE1vZHVsZVxuXG5leHBvcnQge1xuICBzeW5jaHJvbml6ZSxcbiAgc3luY2hyb25pemVkLFxufVxuIiwiLy8gSW50ZXJuYWwgRGVwZW5kZW5jaWVzXG5cbmltcG9ydCB7IE1FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkUgfSBmcm9tICdAL21lc3NhZ2VUeXBlcydcblxuLy8gTW9kdWxlXG5cbmV4cG9ydCBkZWZhdWx0IGhvc3RuYW1lID0+XG4gIGNocm9tZS5ydW50aW1lLnNlbmRNZXNzYWdlKHtcbiAgICB0eXBlOiBNRVNTQUdFX1RZUEVfX1NZTkNIUk9OSVpFLFxuICAgIGhvc3RuYW1lLFxuICB9KVxuIiwiLy8gSW50ZXJuYWwgRGVwZW5kZW5jaWVzXG5cbmltcG9ydCB7IE1FU1NBR0VfVFlQRV9fU1lOQ0hST05JWkVEIH0gZnJvbSAnQC9tZXNzYWdlVHlwZXMnXG5cbi8vIE1vZHVsZVxuXG5leHBvcnQgZGVmYXVsdCBvcmdhbml6YXRpb25zID0+XG4gIGNocm9tZS5ydW50aW1lLnNlbmRNZXNzYWdlKHtcbiAgICB0eXBlOiBNRVNTQUdFX1RZUEVfX1NZTkNIUk9OSVpFRCxcbiAgICBvcmdhbml6YXRpb25zLFxuICB9KVxuIiwiLy8gTW9kdWxlXG5cbmV4cG9ydCBkZWZhdWx0IHN0eWxlID0+IE9iamVjdFxuICAgICAgICAgICAgICAgICAgICAgICAgICAuZW50cmllcyhzdHlsZSlcbiAgICAgICAgICAgICAgICAgICAgICAgICAgLm1hcCgoW2tleSwgdmFsdWVdKSA9PiBgJHtrZXl9OiAke3ZhbHVlfWApXG4gICAgICAgICAgICAgICAgICAgICAgICAgIC5qb2luKCc7ICcpXG4iLCIvLyBJbnRlcm5hbCBEZXBlbmRlbmNpZXNcblxuaW1wb3J0IGNyZWF0ZVN0eWxlQXR0cmlidXRlIGZyb20gJy4vY3JlYXRlU3R5bGVBdHRyaWJ1dGUnXG5cbi8vIE1vZHVsZVxuXG5leHBvcnQgZGVmYXVsdCBkYXRhID0+IHtcbiAgY29uc3Qgb3ZlcmxheSAgICAgICAgICA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXG4gICAgICAsIG92ZXJsYXlUZXh0ICAgICAgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxuICAgICAgLCBvdmVybGF5U3R5bGUgICAgID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAncG9zaXRpb24nOiAgJ2Fic29sdXRlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2xlZnQnOiAgICAgIDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICdyaWdodCc6ICAgICAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAndG9wJzogICAgICAgMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ2JvdHRvbSc6ICAgIDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICd6LWluZGV4JzogICAxMDAwMDAwMDAwMCxcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYmFja2Ryb3AtZmlsdGVyJzogJ2JsdXIoM3B4KScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICdiYWNrZ3JvdW5kJzogICAgICAncmdiYSgyNTUsIDI1NSwgMjU1LCAwLjM4MTk2NiknLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnY29sb3InOiAgICAgICAgICAgJyMwMDAnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnZm9udC1zaXplJzogICAgICAgJzM2cHQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAndGV4dC1zaGFkb3cnOiAgICAgJzAgMCA1cHggI2ZmZicsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAsIG92ZXJsYXlUZXh0U3R5bGUgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICdwb3NpdGlvbic6ICAnYWJzb2x1dGUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAndG9wJzogICAgICAgJzUwJScsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICdsZWZ0JzogICAgICAnMTAlJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3JpZ2h0JzogICAgICcxMCUnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAndHJhbnNmb3JtJzogJ3RyYW5zbGF0ZSgwLCAtNTAlKScsXG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3RleHQtYWxpZ24nOiAnY2VudGVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgLCBvdmVybGF5U3R5bGVBdHRyaWJ1dGUgICAgID0gY3JlYXRlU3R5bGVBdHRyaWJ1dGUob3ZlcmxheVN0eWxlKVxuICAgICAgLCBvdmVybGF5VGV4dFN0eWxlQXR0cmlidXRlID0gY3JlYXRlU3R5bGVBdHRyaWJ1dGUob3ZlcmxheVRleHRTdHlsZSlcblxuXG4gIG92ZXJsYXlUZXh0LnRleHRDb250ZW50ID0gJ1BsZWFzZSB3YWl0J1xuICBvdmVybGF5VGV4dC5zZXRBdHRyaWJ1dGUoJ3N0eWxlJywgb3ZlcmxheVRleHRTdHlsZUF0dHJpYnV0ZSlcblxuICBvdmVybGF5LnNldEF0dHJpYnV0ZSgnc3R5bGUnLCBvdmVybGF5U3R5bGVBdHRyaWJ1dGUpXG4gIG92ZXJsYXkuYXBwZW5kQ2hpbGQob3ZlcmxheVRleHQpXG5cbiAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChvdmVybGF5KVxuXG4gIGRhdGEub3ZlcmxheSA9IHsgdXBkYXRlVGV4dDogdGV4dCA9PiBvdmVybGF5VGV4dC50ZXh0Q29udGVudCA9IHRleHQgfVxuXG4gIHJldHVybiBQcm9taXNlLnJlc29sdmUoKVxufVxuIiwiLy8gTW9kdWxlXG5cbmV4cG9ydCBkZWZhdWx0IGRhdGEgPT4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICBjb25zdCBvcHRpb25zID0ge1xuICAgICAgICAgICAgICAgICAgICBib2R5OiAgICAgICAgICAgbnVsbCxcbiAgICAgICAgICAgICAgICAgICAgY2FjaGU6ICAgICAgICAgICdyZWxvYWQnLFxuICAgICAgICAgICAgICAgICAgICBjcmVkZW50aWFsczogICAgJ2luY2x1ZGUnLFxuICAgICAgICAgICAgICAgICAgICBtZXRob2Q6ICAgICAgICAgJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgIG1vZGU6ICAgICAgICAgICAnY29ycycsXG4gICAgICAgICAgICAgICAgICAgIHJlZmVycmVyOiAgICAgICBgJHtkYXRhLmhvc3RuYW1lfXBsYW5zYCxcbiAgICAgICAgICAgICAgICAgICAgcmVmZXJyZXJQb2xpY3k6ICduby1yZWZlcnJlci13aGVuLWRvd25ncmFkZScsXG4gICAgICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAnYWNjZXB0JzogICAgICAgICAgJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAgICAgICAgICAgICAgICdhY2NlcHQtbGFuZ3VhZ2UnOiAnZW4tVVMsZW47cT0wLjknLFxuICAgICAgICAgICAgICAgICAgICAgICdpZi1ub25lLW1hdGNoJzogICAnVy9cXFwiMTk1ZC10RjNBVldiSjZJekdBVGhJV3VFdTZjWnBIVThcXFwiJyxcbiAgICAgICAgICAgICAgICAgICAgICAnc2VjLWZldGNoLW1vZGUnOiAgJ2NvcnMnLFxuICAgICAgICAgICAgICAgICAgICAgICdzZWMtZmV0Y2gtc2l0ZSc6ICAnc2FtZS1vcmlnaW4nLFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgfVxuICAgICAgLCB1cmwgICAgID0gYCR7ZGF0YS5ob3N0bmFtZX1tYW5hZ2VyL2FwaS9wdWJsaWMvb3JnYW5pemF0aW9ucy9wZXJtaXNzaW9uc2BcbiAgICAgICAgICAgICAgICArIGA/YWN0aW9uPXJlYWQmcmVzb3VyY2U9cGxhbmBcblxuICBjb25zb2xlLmxvZyh7dXJsfSlcblxuICBkYXRhLm92ZXJsYXkudXBkYXRlVGV4dCgnRmV0Y2hpbmcgb3JnYW5pemF0aW9ucy4uLicpXG5cbiAgZmV0Y2godXJsLCBvcHRpb25zKVxuICAgIC50aGVuKHJlc3BvbnNlID0+IHJlc3BvbnNlLmpzb24oKSlcbiAgICAudGhlbihvcmdhbml6YXRpb25zID0+IHtcbiAgICAgIGRhdGEub3JnYW5pemF0aW9ucyA9IG9yZ2FuaXphdGlvbnMucmVkdWNlKFxuICAgICAgICAob3JnYW5pemF0aW9uTWFwLCBvcmdhbml6YXRpb24pID0+IHtcbiAgICAgICAgICBvcmdhbml6YXRpb25NYXBbb3JnYW5pemF0aW9uLmlkXSA9IG9yZ2FuaXphdGlvblxuXG4gICAgICAgICAgcmV0dXJuIG9yZ2FuaXphdGlvbk1hcFxuICAgICAgICB9LFxuICAgICAgICB7fSxcbiAgICAgIClcblxuICAgICAgcmVzb2x2ZSgpXG4gICAgfSlcbiAgICAuY2F0Y2goZXJyb3IgPT4gY29uc29sZS5sb2coJ2ZldGNoT3JnYW5pemF0aW9ucyBlcnJvcicsIGVycm9yKSlcbn0pXG4iLCIvLyBNb2R1bGVcblxuY29uc3QgZmV0Y2hQbGFuID0gKGRhdGEsIG9yZ2FuaXphdGlvbiwgcmVtYWluaW5nKSA9PiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gIGNvbnN0IG9wdGlvbnMgPSB7XG4gICAgICAgICAgICAgICAgICAgIGJvZHk6ICAgICAgICAgICBudWxsLFxuICAgICAgICAgICAgICAgICAgICBjcmVkZW50aWFsczogICAgJ2luY2x1ZGUnLFxuICAgICAgICAgICAgICAgICAgICBtZXRob2Q6ICAgICAgICAgJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgIG1vZGU6ICAgICAgICAgICAnY29ycycsXG4gICAgICAgICAgICAgICAgICAgIHJlZmVycmVyOiAgICAgICBgJHtkYXRhLmhvc3RuYW1lfXBsYW5zLyR7b3JnYW5pemF0aW9uLmlkfS9saXN0YCxcbiAgICAgICAgICAgICAgICAgICAgcmVmZXJyZXJQb2xpY3k6ICduby1yZWZlcnJlci13aGVuLWRvd25ncmFkZScsXG4gICAgICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAnYWNjZXB0JzogICAgICAgICAgJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAgICAgICAgICAgICAgICdhY2NlcHQtbGFuZ3VhZ2UnOiAnZW4tVVMsZW47cT0wLjknLFxuICAgICAgICAgICAgICAgICAgICAgICdzZWMtZmV0Y2gtbW9kZSc6ICAnY29ycycsXG4gICAgICAgICAgICAgICAgICAgICAgJ3NlYy1mZXRjaC1zaXRlJzogICdzYW1lLW9yaWdpbidcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICwgdXJsICAgICA9IGAke2RhdGEuaG9zdG5hbWV9bWFuYWdlci9hcGkvcGxhbnNgXG4gICAgICAgICAgICAgICAgKyBgP29yZ2FuaXphdGlvbklkPSR7b3JnYW5pemF0aW9uLmlkfWBcblxuICBkYXRhLm92ZXJsYXkudXBkYXRlVGV4dChgRmV0Y2hpbmcgcGxhbnMgZm9yICR7b3JnYW5pemF0aW9uLm5hbWV9Li4uYClcblxuICBmZXRjaCh1cmwsIG9wdGlvbnMpXG4gICAgLnRoZW4ocmVzcG9uc2UgPT4gcmVzcG9uc2UuanNvbigpKVxuICAgIC50aGVuKHBsYW5zID0+IHtcbiAgICAgIG9yZ2FuaXphdGlvbi5wbGFucyA9IHBsYW5zLnJlZHVjZShcbiAgICAgICAgKHBsYW5NYXAsIHBsYW4pID0+IHtcbiAgICAgICAgICBwbGFuTWFwW3BsYW4uaWRdID0gcGxhblxuXG4gICAgICAgICAgcmV0dXJuIHBsYW5NYXBcbiAgICAgICAgfSxcbiAgICAgICAge30sXG4gICAgICApXG5cbiAgICAgIGlmIChyZW1haW5pbmcubGVuZ3RoKSB7XG4gICAgICAgIGNvbnN0IG5leHRPcmdhbml6YXRpb24gPSByZW1haW5pbmcuc2hpZnQoKVxuXG4gICAgICAgIGZldGNoUGxhbihkYXRhLCBuZXh0T3JnYW5pemF0aW9uLCByZW1haW5pbmcpXG4gICAgICAgICAgLnRoZW4ocmVzb2x2ZSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlc29sdmUoKVxuICAgICAgfVxuICAgIH0pXG4gIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZygnZmV0Y2hQbGFuIGVycm9yJywgZXJyb3IpKVxufSlcblxuZXhwb3J0IGRlZmF1bHQgZmV0Y2hQbGFuXG4iLCIvLyBJbnRlcm5hbCBEZXBlbmRlbmNpZXNcblxuaW1wb3J0IGZldGNoUGxhbiBmcm9tICcuL2ZldGNoUGxhbidcblxuLy8gTW9kdWxlXG5cbmV4cG9ydCBkZWZhdWx0IGRhdGEgPT4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICBjb25zdCByZW1haW5pbmcgPSBPYmplY3QudmFsdWVzKGRhdGEub3JnYW5pemF0aW9ucylcblxuICAgICAgLCBvcmdhbml6YXRpb24gPSByZW1haW5pbmcuc2hpZnQoKVxuXG4gIGZldGNoUGxhbihkYXRhLCBvcmdhbml6YXRpb24sIHJlbWFpbmluZykudGhlbihyZXNvbHZlKVxufSlcbiIsIi8vIE1vZHVsZVxuXG5leHBvcnQgZGVmYXVsdCBkYXRhID0+IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgY29uc3QgdXJsID0gbmV3IFVSTCh3aW5kb3cubG9jYXRpb24uaHJlZilcblxuICB1cmwucGF0aG5hbWUgPSAnJ1xuXG4gIGRhdGEuaG9zdG5hbWUgPSB1cmwudG9TdHJpbmcoKVxuXG4gIHJlc29sdmUoKVxufSlcbiIsIi8vIEludGVybmFsIERlcGVuZGVuY2llc1xuXG5pbXBvcnQgY3JlYXRlT3ZlcmxheSAgICAgIGZyb20gJy4vY3JlYXRlT3ZlcmxheSdcbmltcG9ydCBnZXRIb3N0bmFtZSAgICAgICAgZnJvbSAnLi9nZXRIb3N0bmFtZSdcbmltcG9ydCBmZXRjaE9yZ2FuaXphdGlvbnMgZnJvbSAnLi9mZXRjaE9yZ2FuaXphdGlvbnMnXG5pbXBvcnQgZmV0Y2hQbGFucyAgICAgICAgIGZyb20gJy4vZmV0Y2hQbGFucydcblxuaW1wb3J0IHsgc3luY2hyb25pemVkIH0gZnJvbSAnQC9tZXNzYWdlcydcblxuLy8gTW9kdWxlXG5cbmNvbnN0IGRhdGEgPSB7XG4gIGhvc3RuYW1lOiAgICAgICcnLFxuICBvcmdhbml6YXRpb25zOiBbXSxcbn1cblxuY3JlYXRlT3ZlcmxheShkYXRhKVxuICAudGhlbigoKSA9PiBnZXRIb3N0bmFtZShkYXRhKSlcbiAgLnRoZW4oKCkgPT4gZmV0Y2hPcmdhbml6YXRpb25zKGRhdGEpKVxuICAudGhlbigoKSA9PiBmZXRjaFBsYW5zKGRhdGEpKVxuICAudGhlbigoKSA9PiB7XG4gICAgY29uc29sZS5sb2coJ0FsbCBwbGFuIGRhdGEgZmV0Y2hlZCcsIGRhdGEpXG5cbiAgICBkYXRhLm92ZXJsYXkudXBkYXRlVGV4dCgnQ29tcGxldGUnKVxuXG4gICAgc3luY2hyb25pemVkKGRhdGEub3JnYW5pemF0aW9ucylcblxuICAgIHdpbmRvdy5jbG9zZSgpXG4gIH0pXG4iXSwic291cmNlUm9vdCI6IiJ9