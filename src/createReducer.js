// Module

export default (actionHandlers, defaultState) => (state = defaultState, action) => {
  if (Object.keys(actionHandlers).includes(action.type))
    return actionHandlers[action.type](state, action)

  return state
}
