// Internal Dependencies

import messageHandlers from './messageHandlers'

// Module

const data = {
  organizations: [],
}

chrome.storage.local.get(
  ['organizations'],
  ({organizations}) => data.organizations = organizations || [],
)

chrome.runtime.onMessage.addListener(message => {
  console.log('background message', message)

  if (!Object.keys(messageHandlers).includes(message.type)) return

  messageHandlers[message.type](data, message)

  console.log('new background state', data)
})
