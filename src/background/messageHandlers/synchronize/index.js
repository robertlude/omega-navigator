// Internal Dependencies

import createUpdater from './createUpdater'

// Module

export default (data, {hostname}) => {
  chrome.tabs.create(
    {
      url: `${hostname}plans`,
    },
    tab => {
      const updater = createUpdater(tab.id)

      chrome.tabs.onUpdated.addListener(updater)
    }
  )
}
