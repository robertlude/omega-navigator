// Module

export default tabId => (eventTabId, info) => {
  if (tabId != eventTabId) return

  if (info.status != 'complete') return

  chrome.tabs.executeScript(tabId, {file: 'synchronizeOrchestration.js'})
}
