// Internal Dependencies

import synchronize  from './synchronize'
import synchronized from './synchronized'

import {
  MESSAGE_TYPE__SYNCHRONIZE,
  MESSAGE_TYPE__SYNCHRONIZED,
} from '@/messageTypes'

// Module

export default {
  [MESSAGE_TYPE__SYNCHRONIZE]:  synchronize,
  [MESSAGE_TYPE__SYNCHRONIZED]: synchronized,
}
