// Module

export default (data, {organizations}) => {
  data.organizations = organizations

  chrome.storage.local.set(
    {organizations},
    () => console.log('organizations saved')
  )
}
