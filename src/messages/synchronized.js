// Internal Dependencies

import { MESSAGE_TYPE__SYNCHRONIZED } from '@/messageTypes'

// Module

export default organizations =>
  chrome.runtime.sendMessage({
    type: MESSAGE_TYPE__SYNCHRONIZED,
    organizations,
  })
