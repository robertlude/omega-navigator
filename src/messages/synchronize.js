// Internal Dependencies

import { MESSAGE_TYPE__SYNCHRONIZE } from '@/messageTypes'

// Module

export default hostname =>
  chrome.runtime.sendMessage({
    type: MESSAGE_TYPE__SYNCHRONIZE,
    hostname,
  })
