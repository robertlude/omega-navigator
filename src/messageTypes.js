// Module

const MESSAGE_TYPE__SET_ORGANIZATIONS = 'messageType:setOrganizations'
    , MESSAGE_TYPE__SYNCHRONIZE       = 'messageType:synchronize'
    , MESSAGE_TYPE__SYNCHRONIZED      = 'messageType:synchronized'

export {
  MESSAGE_TYPE__SET_ORGANIZATIONS,
  MESSAGE_TYPE__SYNCHRONIZE,
  MESSAGE_TYPE__SYNCHRONIZED,
}
