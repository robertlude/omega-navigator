// Module

export default label => store => next => action => {
  console.log(`${label} action dispatching`, action)

  const result = next(action)

  console.log(`${label} state`, store.getState())

  return result
}
