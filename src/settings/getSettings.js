// Internal Dependencies

import store from 's/store'

import {
  setAlphaSuiteHostname,
  setOmegaSuiteHostname,
  setOrchestrationHostname,
} from 's/actions'

// Module

export default () =>
  chrome.storage.local.get(
    [
      'alphaSuiteHostname',
      'omegaSuiteHostname',
      'orchestrationHostname',
    ],
    ({
      alphaSuiteHostname,
      omegaSuiteHostname,
      orchestrationHostname,
    }) => {
      store.dispatch(setAlphaSuiteHostname(alphaSuiteHostname || ''))
      store.dispatch(setOmegaSuiteHostname(omegaSuiteHostname || ''))
      store.dispatch(setOrchestrationHostname(orchestrationHostname || ''))
    }
  )
