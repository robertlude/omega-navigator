// Internal Dependencies

import setAlphaSuiteHostname    from './setAlphaSuiteHostname'
import setOmegaSuiteHostname    from './setOmegaSuiteHostname'
import setOrchestrationHostname from './setOrchestrationHostname'

// Module

export {
  setAlphaSuiteHostname,
  setOmegaSuiteHostname,
  setOrchestrationHostname,
}
