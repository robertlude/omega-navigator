// Internal Dependencies

import { ACTION_TYPE__SET_ORCHESTRATION_HOSTNAME } from 's/actionTypes'

// Module

export default hostname => ({
  type: ACTION_TYPE__SET_ORCHESTRATION_HOSTNAME,
  hostname,
})
