// Module

export default ({
  alphaSuiteHostname,
  omegaSuiteHostname,
  orchestrationHostname,
}) => ({
  alphaSuiteHostname,
  omegaSuiteHostname,
  orchestrationHostname,
})
