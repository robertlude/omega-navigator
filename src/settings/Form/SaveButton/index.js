// External Dependencies

import { connect } from 'react-redux'
import { Button }  from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import stateProps from './stateProps'

import closeSettings from '../closeSettings'

// Module

class SaveButton extends Component {
  render() {
    const {
            alphaSuiteHostname,
            omegaSuiteHostname,
            orchestrationHostname,
          } = this.props

    return <Button
      color='primary'
      onClick={() => {
        chrome.storage.local.set({
          alphaSuiteHostname,
          omegaSuiteHostname,
          orchestrationHostname,
        })

        closeSettings()
      }}
    >
      Save
    </Button>
  }
}

export default connect(stateProps)(SaveButton)
