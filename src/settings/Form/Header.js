// External Dependencies

import { Typography } from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Module

class Header extends Component {
  render() {
    return <Typography
      component='h1'
      variant  ='h4'
    >
      Omega Navigator Settings
    </Typography>
  }
}

export default Header
