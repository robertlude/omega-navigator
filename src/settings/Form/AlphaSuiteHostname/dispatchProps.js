// Internal Dependencies

import { setAlphaSuiteHostname } from 's/actions'

// Module

export default ({
  setAlphaSuiteHostname,
})
