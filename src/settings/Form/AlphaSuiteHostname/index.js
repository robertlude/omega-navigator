// External Dependencies

import { connect } from 'react-redux'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './dispatchProps'
import stateProps    from './stateProps'

import HostnameField from '../HostnameField'

// Module

class AlphaSuiteHostname extends Component {
  render() {
    const {
            alphaSuiteHostname,
            setAlphaSuiteHostname,
          } = this.props

    return <HostnameField
      label   ='AlphaSuite'
      onChange={event => setAlphaSuiteHostname(event.target.value)}
      value   ={alphaSuiteHostname}
    />
  }
}

export default connect(
  stateProps,
  dispatchProps,
)(AlphaSuiteHostname)
