// External Dependencies

import { TextField } from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Module

class HostnameField extends Component {
  render() {
    const {
            label,
            onChange,
            value,
          } = this.props

    return <TextField
             label      ={`${label} Hostname`}
             onChange   ={onChange}
             placeholder='https://www.example.com'
             value      ={value}
             fullWidth
           />
  }
}

export default HostnameField
