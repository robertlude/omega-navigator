// External Dependencies

import { connect } from 'react-redux'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './dispatchProps'
import stateProps    from './stateProps'

import HostnameField from '../HostnameField'

// Module

class OmegaSuiteHostname extends Component {
  render() {
    const {
            omegaSuiteHostname,
            setOmegaSuiteHostname,
          } = this.props

    return <HostnameField
      label   ='OmegaSuite'
      onChange={event => setOmegaSuiteHostname(event.target.value)}
      value   ={omegaSuiteHostname}
    />
  }
}

export default connect(
  stateProps,
  dispatchProps,
)(OmegaSuiteHostname)
