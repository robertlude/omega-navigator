// Internal Dependencies

import { setOmegaSuiteHostname } from 's/actions'

// Module

export default ({
  setOmegaSuiteHostname,
})
