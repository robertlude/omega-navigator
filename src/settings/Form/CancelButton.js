// External Dependencies

import { Button } from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Depedencies

import closeSettings from './closeSettings'

// Module

class CancelButton extends Component {
  render() {
    return <Button onClick={closeSettings}>Cancel</Button>
  }
}

export default CancelButton
