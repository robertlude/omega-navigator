// External Dependencies

import { Typography } from '@material-ui/core'
import { connect }    from 'react-redux'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import AlphaSuiteHostname    from './AlphaSuiteHostname'
import CancelButton          from './CancelButton'
import Header                from './Header'
import OmegaSuiteHostname    from './OmegaSuiteHostname'
import OrchestrationHostname from './OrchestrationHostname'
import SaveButton            from './SaveButton'

// Module

class Form extends Component {
  render() {
    return <div>
      <Header />
      <AlphaSuiteHostname />
      <OmegaSuiteHostname />
      <OrchestrationHostname />
      <SaveButton />
      <CancelButton />
    </div>
  }
}

export default Form
