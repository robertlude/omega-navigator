// External Dependencies

import { connect } from 'react-redux'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './dispatchProps'
import stateProps    from './stateProps'

import HostnameField from '../HostnameField'

// Module

class OrchestrationHostname extends Component {
  render() {
    const {
            orchestrationHostname,
            setOrchestrationHostname,
          } = this.props

    return <HostnameField
      label   ='Orchestration'
      onChange={event => setOrchestrationHostname(event.target.value)}
      value   ={orchestrationHostname}
    />
  }
}

export default connect(
  stateProps,
  dispatchProps,
)(OrchestrationHostname)
