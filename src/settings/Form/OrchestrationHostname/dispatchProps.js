// Internal Dependencies

import { setOrchestrationHostname } from 's/actions'

// Module

export default ({
  setOrchestrationHostname,
})
