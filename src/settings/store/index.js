// External Dependencies

import {
  applyMiddleware,
  createStore,
} from 'redux'

// Internal Dependencies

import reducer from './reducer'

import actionLogger from '@/actionLogger'

// Module

export default createStore(
  reducer,
  applyMiddleware(actionLogger('settings')),
)
