// Module

export default (state, {hostname}) => ({
  ...state,
  omegaSuiteHostname: hostname,
})
