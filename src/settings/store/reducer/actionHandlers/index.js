// Internal Dependencies

import setAlphaSuiteHostname    from './setAlphaSuiteHostname'
import setOmegaSuiteHostname    from './setOmegaSuiteHostname'
import setOrchestrationHostname from './setOrchestrationHostname'

import {
  ACTION_TYPE__SET_ALPHA_SUITE_HOSTNAME,
  ACTION_TYPE__SET_OMEGA_SUITE_HOSTNAME,
  ACTION_TYPE__SET_ORCHESTRATION_HOSTNAME,
} from 's/actionTypes'

// Module

export default {
  [ACTION_TYPE__SET_ALPHA_SUITE_HOSTNAME]:   setAlphaSuiteHostname,
  [ACTION_TYPE__SET_OMEGA_SUITE_HOSTNAME]:   setOmegaSuiteHostname,
  [ACTION_TYPE__SET_ORCHESTRATION_HOSTNAME]: setOrchestrationHostname,
}
