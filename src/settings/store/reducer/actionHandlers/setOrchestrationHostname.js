// Module

export default (state, {hostname}) => ({
  ...state,
  orchestrationHostname: hostname,
})
