// Module

export default (state, {hostname}) => ({
  ...state,
  alphaSuiteHostname: hostname,
})
