// Internal Dependencies

import actionHandlers from './actionHandlers'

import createReducer from '@/createReducer'

// Module

export default createReducer(
  actionHandlers,
  {
    alphaSuiteHostname:    '',
    omegaSuiteHostname:    '',
    orchestrationHostname: '',
  },
)
