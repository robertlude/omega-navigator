// Internal Dependencies

import getSettings from './getSettings'
import render      from './render'

// Module

getSettings()
render()
