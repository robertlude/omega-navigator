// External Dependencies

import React        from 'react'
import ReactDOM     from 'react-dom'
import { Provider } from 'react-redux'

// Internal Dependencies

import Form from './Form'

import store from 's/store'

// Module

export default () =>
  ReactDOM.render(
    <Provider store={store}>
      <Form />
    </Provider>,
    document.getElementById('container'),
  )
