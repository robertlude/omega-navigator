// Internal Dependencies

import { setHostnames } from 'p/actions'

import store from 'p/store'

// Module

export default () => {
  chrome.storage.local.get(
    [
      'alphaSuiteHostname',
      'omegaSuiteHostname',
      'orchestrationHostname',
    ],
    ({
      alphaSuiteHostname,
      omegaSuiteHostname,
      orchestrationHostname,
    }) => {
      store.dispatch(setHostnames({
        alphaSuite:    alphaSuiteHostname    || '',
        omegaSuite:    omegaSuiteHostname    || '',
        orchestration: orchestrationHostname || '',
      }))
    }
  )
}
