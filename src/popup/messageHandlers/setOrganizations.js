// Internal Dependencies

import { setOrganizations } from 'p/actions'

// Module

export default (store, message) =>
  store.dispatch(setOrganizations(message.organizations))
