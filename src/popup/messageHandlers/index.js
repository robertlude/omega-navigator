// Internal Dependencies

import setOrganizations from './setOrganizations'

import {
  MESSAGE_TYPE__SET_ORGANIZATIONS,
} from '@/messageTypes'

// Module

export default {
  [MESSAGE_TYPE__SET_ORGANIZATIONS]: setOrganizations,
}
