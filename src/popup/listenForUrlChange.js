// Internal Dependencies

import getUrl from './getUrl'

// Module

export default () => chrome.tabs.onUpdated.addListener(getUrl)
