// Internal Dependencies

import AlphaSuiteOrganization    from './AlphaSuiteOrganization'
import OmegaSuitePlan            from './OmegaSuitePlan'
import OrchestrationOrganization from './OrchestrationOrganization'
import OrchestrationPlan         from './OrchestrationPlan'

// Module

export default {
  organizations: [
    OrchestrationOrganization,
    AlphaSuiteOrganization,
  ],
  plans: [
    OrchestrationPlan,
    OmegaSuitePlan,
  ],
}
