// Internal Dependencies

import escapeHostnameRegex from './escapeHostnameRegex'

import getFirstAlphabetically from 'p/getFirstAlphabetically'

// Module

export default class Organization {
  constructor(hostnames, organizationId) {
    this.hostname       = hostnames[this.constructor.hostnameKey]
    this.organizationId = organizationId
  }

  static getIds(hostnames, url, organizations) {
    const hostname = escapeHostnameRegex(hostnames[this.hostnameKey])

        , match = new RegExp(`${hostname}${this.urlRegexSuffix}`).exec(url)

    if (!match) return {found: false}

    const organizationId = match[1]

        , organization = organizations[organizationId]

    return {
      found: true,
      data: {
        appName:  this.appName,
        health:   `${hostnames[this.hostnameKey]}health`,
        oliveApp: true,
        planId:   getFirstAlphabetically(organization.plans).id,
        organizationId,
      },
    }
  }
}
