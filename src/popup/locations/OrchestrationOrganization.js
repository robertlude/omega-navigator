// External Dependencies

import LaunchIcon from '@material-ui/icons/Launch'

import React from 'react'

// Internal Dependencies

import OrganizationButtons from './OrganizationButtons'

// Module

export default class OrchestrationOrganization extends OrganizationButtons {
  links() {
    return [
      {
        icon:  <LaunchIcon />,
        label: 'Orchestration',
        url:   `${this.hostname}plans/${this.organizationId}/list`,
      },
    ]
  }
}

OrchestrationOrganization.appName        = 'Orchestration'
OrchestrationOrganization.hostnameKey    = 'orchestration'
OrchestrationOrganization.urlRegexSuffix = 'plans\\/([a-f0-9-]+)\\/list$'
