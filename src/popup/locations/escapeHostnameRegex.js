// Module

export default hostname =>
  `^` + hostname
          .replace(/\//g, '\\/')
          .replace(/\./g, '\\.')
