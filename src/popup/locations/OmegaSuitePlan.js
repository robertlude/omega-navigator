// External Dependencies

import React from 'react'

// Internal Dependencies

import PlanButtons from './PlanButtons'

// Module

export default class OmegaSuitePlan extends PlanButtons {
  links() {
    return [
      {
        icon:  '\u03A9',
        label: 'OmegaSuite',
        url:   `${this.hostname}watch/${this.planId}?tab=Summary`,
      },
    ]
  }
}

OmegaSuitePlan.appName        = 'OmegaSuite'
OmegaSuitePlan.hostnameKey    = 'omegaSuite'
OmegaSuitePlan.urlRegexSuffix = 'watch\\/([a-f0-9-]+)'
