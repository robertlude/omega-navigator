// External Dependencies

import {
  IconButton,
  Menu,
  MenuItem,
  Tooltip,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import createNavigationEvent from './createNavigationEvent'

// Module

class NavigationMenu extends Component {
  constructor(props) {
    super(props)

    this.state = {anchor: null}
  }

  render() {
    const { anchor } = this.state
        , {
            icon,
            tooltip,
            urls,
          } = this.props

        , handleClick = event => this.setState({anchor: event.currentTarget})
        , handleClose = () => this.setState({anchor: undefined})

    return <div>
      <Tooltip
        title={tooltip}
        arrow
      >
        <IconButton onClick={handleClick}>
          {icon}
        </IconButton>
      </Tooltip>
      <Menu
        anchorEl={anchor}
        open    ={Boolean(anchor)}
        onClose ={handleClose}
        keepMounted
      >
        {Object.entries(urls).map(([label, url]) => {
          return <MenuItem
            key    ={url}
            onClick={createNavigationEvent(url)}
          >
            {label}
          </MenuItem>
        })}
      </Menu>
    </div>
  }
}

export default NavigationMenu
