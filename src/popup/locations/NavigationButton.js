// External Dependencies

import {
  IconButton,
  Tooltip,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import createNavigationEvent from './createNavigationEvent'

// Module

class NavigationButton extends Component {
  render() {
    const {
            icon,
            tooltip,
            url,
          } = this.props

    return <Tooltip
      title={tooltip}
      arrow
    >
      <IconButton onClick={createNavigationEvent(url)}>
        {icon}
      </IconButton>
    </Tooltip>
  }
}

export default NavigationButton
