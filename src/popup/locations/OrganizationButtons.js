// External Dependencies

import React from 'react'

// Internal Dependencies

import Organization     from './Organization'
import NavigationButton from './NavigationButton'
import NavigationMenu   from './NavigationMenu'

// Module

export default class OrganizationButtons extends Organization {
  constructor(hostname, organizationId) {
    super(hostname)

    this.organizationId = organizationId
  }

  render() {
    return <div key={this.constructor.name}>
             {this.links().map(link =>
               <NavigationButton
                 key    ={link.url}
                 icon   ={link.icon}
                 tooltip={link.label}
                 url    ={link.url}
               />
             )}
           </div>
  }
}
