// Internal Dependencies

import escapeHostnameRegex from './escapeHostnameRegex'

// Module

export default class Plan {
  constructor(hostnames, organizationId, planId) {
    this.hostname       = hostnames[this.constructor.hostnameKey]
    this.organizationId = organizationId
    this.planId         = planId
  }

  static getIds(hostnames, url, organizations) {
    const hostname = escapeHostnameRegex(hostnames[this.hostnameKey])

        , match = new RegExp(`${hostname}${this.urlRegexSuffix}`).exec(url)

    if (!match) return {found: false}

    const planId = match[1]

    for (let organization of Object.values(organizations)) {
      if (Object.keys(organization.plans).includes(planId))
        return {
          found: true,
          data: {
            appName:        this.appName,
            health:         `${hostnames[this.hostnameKey]}health`,
            oliveApp:       true,
            organizationId: organization.id,
            planId:         planId,
          }
        }
    }
  }
}
