// External Dependencies

import React from 'react'

import EditIcon      from '@material-ui/icons/Edit'
import HistoryIcon   from '@material-ui/icons/History'
import PlayArrowIcon from '@material-ui/icons/PlayArrow'

// Internal Dependencies

import PlanButtons from './PlanButtons'

// Module

export default class OrchestrationPlan extends PlanButtons {
  links() {
    const baseUrl = `${this.hostname}plans/${this.organizationId}`
                  + `/${this.planId}`

    return [
      {
        icon:  <HistoryIcon />,
        label: 'History',
        url:   `${baseUrl}/history`,
      },
      {
        icon:  <EditIcon />,
        label: 'Edit',
        url:   `${baseUrl}/edit`,
      },
      {
        icon:  <PlayArrowIcon />,
        label: 'Run',
        url:   `${baseUrl}/start`,
      },
    ]
  }
}

OrchestrationPlan.appName        = 'Orchestration'
OrchestrationPlan.hostnameKey    = 'orchestration'
OrchestrationPlan.urlRegexSuffix = 'plans\\/(?:[a-f0-9-]+)\\/([a-f0-9-]+)'
