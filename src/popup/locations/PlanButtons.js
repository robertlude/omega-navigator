// External Dependencies

import React from 'react'

// Internal Dependencies

import NavigationButton from './NavigationButton'
import NavigationMenu   from './NavigationMenu'
import Plan             from './Plan'

// Module

export default class PlanButtons extends Plan {
  render() {
    return <div key={this.constructor.name}>
             {this.links().map(link =>
               <NavigationButton
                 key    ={link.url}
                 icon   ={link.icon}
                 tooltip={link.label}
                 url    ={link.url}
               />
             )}
           </div>
  }
}
