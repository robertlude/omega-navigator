// External Dependencies

import React from 'react'

// Internal Dependencies

import Organization     from './Organization'
import NavigationButton from './NavigationButton'
import NavigationMenu   from './NavigationMenu'

// Module

export default class OrganizationMenu extends Organization {
  render() {
    return <NavigationMenu
      key    ={this.constructor.name}
      icon   ={this.constructor.icon}
      tooltip={this.constructor.appName}
      urls   ={this.links()}
    />
  }
}
