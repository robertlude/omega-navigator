// External Dependencies

import LaunchIcon from '@material-ui/icons/Launch'

import React from 'react'

// Internal Dependencies

import OrganizationMenu from './OrganizationMenu'

// Module

export default class AlphaSuiteOrganization extends OrganizationMenu {
  links() {
    const alphaSuiteBase = `${this.hostname}accounts/${this.organizationId}`

    return {
      'Overview':    alphaSuiteBase,
      'CPA Builder': `${alphaSuiteBase}/cpa-builder`,
      'Credentials': `${alphaSuiteBase}/credentials`,
      'Files':       `${alphaSuiteBase}/files`,
      'Contacts':    `${alphaSuiteBase}/contacts`,
    }
  }
}

AlphaSuiteOrganization.appName        = 'AlphaSuite'
AlphaSuiteOrganization.hostnameKey    = 'alphaSuite'
AlphaSuiteOrganization.icon           = '\u0391'
AlphaSuiteOrganization.urlRegexSuffix = 'accounts\\/([a-f0-9-]+)'
