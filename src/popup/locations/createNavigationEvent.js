// Module

export default url => event => {
  if (event.shiftKey)
    return chrome.windows.create({url})

  if (event.metaKey)
    return chrome.tabs.create({url})

  chrome.tabs.query(
    {
      active:        true,
      currentWindow: true,
    },
    tab => chrome.tabs.update(tab.id, {url}),
  )
}
