// External Dependencies

import React        from 'react'
import ReactDOM     from 'react-dom'
import { Provider } from 'react-redux'

// Internal Dependencies

import AppRoot from 'p/AppRoot'
import store   from 'p/store'

// Module

export default () =>
  ReactDOM.render(
    <Provider store={store}>
      <AppRoot />
    </Provider>,
    document.getElementById('content-container'),
  )
