// Internal Dependencies

import store      from 'p/store'
import { setUrl } from 'p/actions'

// Module

export default () =>
  chrome.tabs.query(
    {
      active:        true,
      currentWindow: true,
    },
    tabs => store.dispatch(setUrl(tabs[0].url)),
  )
