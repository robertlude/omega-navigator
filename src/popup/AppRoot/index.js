// External Dependencies

import { connect } from 'react-redux'

import {
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  Typography,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Loading    from './Loading'
import Navigator  from './Navigator'
import stateProps from './stateProps'

// Module

class AppRoot extends Component {
  render() {
    const { ready } = this.props

    if (ready) {
      return <div>
        <Typography variant='h5' component='h1'>Omega Navigator</Typography>
        <Navigator />
      </div>
    } else {
      return <Loading />
    }
  }
}

export default connect(stateProps)(AppRoot)
