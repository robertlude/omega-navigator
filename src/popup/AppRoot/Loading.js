// External Dependencies

import React, {
  Component,
} from 'react'

// Module

class Loading extends Component {
  render() {
    return 'Please wait...'
  }
}

export default Loading
