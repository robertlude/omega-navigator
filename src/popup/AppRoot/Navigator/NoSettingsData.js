// External Dependencies

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Controls from './Controls'

// Module

class NoSettingsData extends Component {
  render() {
    return <div>
      No settings data found.
      Configure to try again.
      <Controls />
    </div>
  }
}

export default NoSettingsData
