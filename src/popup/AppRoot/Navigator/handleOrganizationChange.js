// Internal Dependencies

import getFirstAlphabetically from 'p/getFirstAlphabetically'

// Module

export default component => event => {
  const organizationId    = event.target.value
      , { organizations } = component.props

      , organization = organizations[organizationId]

      , planId = getFirstAlphabetically(organization.plans).id

  component.setState({
    organizationId,
    planId,
  })
}
