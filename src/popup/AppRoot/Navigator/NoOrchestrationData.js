// External Dependencies

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Controls from './Controls'

// Module

class NoOrchestrationData extends Component {
  render() {
    return <div>
      No Orchestration data found.
      Synchronize with Orchestration to continue.
      <Controls />
    </div>
  }
}

export default NoOrchestrationData
