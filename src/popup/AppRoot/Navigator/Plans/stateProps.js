// Module

export default ({
  hostnames,
  organizationId,
  organizations,
  planId,
}) => ({
  hostnames,
  organizationId,
  organizations,
  planId,
})
