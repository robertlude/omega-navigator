// External Dependencies

import { connect } from 'react-redux'

import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './dispatchProps'
import stateProps    from './stateProps'

import locations  from 'p/locations'
import sortByName from 'p/sortByName'

// Module

class Plans extends Component {
  render() {
    const {
            hostnames,
            organizationId,
            organizations,
            planId,
            setPlanId,
          } = this.props

        , plans = organizations[organizationId].plans

    return <div style={{display: 'flex'}}>
      <FormControl style={{flex: 1}}>
        <InputLabel id='plan-select-label'>Plan</InputLabel>
        <Select
          labelId ='plan-select-label'
          onChange={event => setPlanId(event.target.value)}
          value   ={planId}
        >
          {sortByName(plans).map(([key, {name}]) =>
            <MenuItem
              key  ={key}
              value={key}
            >
              {name}
            </MenuItem>
          )}
        </Select>
      </FormControl>
      {locations.plans.map(Location =>
        new Location(hostnames, organizationId, planId).render()
      )}
    </div>
  }
}

export default connect(
  stateProps,
  dispatchProps,
)(Plans)
