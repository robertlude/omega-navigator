// External Dependencies

import { connect } from 'react-redux'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import stateProps from './stateProps'

// Module

class Health extends Component {
  render() {
    const {
            appName,
            appVersion,
            health,
          } = this.props

    if (!health) return null

    const style = {
            position: 'absolute',
            left:     10,
            bottom:   10,
          }

    return <div style={style}>
      {`Current location: ${appName || ''} ${appVersion || ''}`}
    </div>
  }
}

export default connect(stateProps)(Health)
