// Module

export default ({
  appName,
  appVersion,
  health,
}) => ({
  appName,
  appVersion,
  health,
})
