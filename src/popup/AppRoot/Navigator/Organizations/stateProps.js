// Module

export default ({
  hostnames,
  organizationId,
  organizations,
}) => ({
  hostnames,
  organizationId,
  organizations,
})
