// External Dependencies

import { connect } from 'react-redux'

import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import dispatchProps from './dispatchProps'
import stateProps    from './stateProps'

import locations  from 'p/locations'
import sortByName from 'p/sortByName'

// Module

class Organizations extends Component {
  render() {
    const {
            hostnames,
            organizationId,
            organizations,
            setOrganizationId,
          } = this.props

    return <div style={{display: 'flex'}}>
      <FormControl style={{flex: 1}}>
        <InputLabel id='organization-select-label'>Organization</InputLabel>
        <Select
          labelId ='organization-select-label'
          onChange={event => setOrganizationId(event.target.value)}
          value   ={organizationId}
        >
          {sortByName(organizations).map(([key, {name}]) =>
            <MenuItem
              key  ={key}
              value={key}
            >
              {name}
            </MenuItem>
          )}
        </Select>
      </FormControl>
      {locations.organizations.map(Location =>
        new Location(hostnames, organizationId).render()
      )}
    </div>
  }
}

export default connect(
  stateProps,
  dispatchProps,
)(Organizations)
