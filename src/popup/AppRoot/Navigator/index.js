// External Dependencies

import { Grid }    from '@material-ui/core'
import { connect } from 'react-redux'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import Controls                 from './Controls'
import Health                   from './Health'
import NoOrchestrationData      from './NoOrchestrationData'
import NoSettingsData           from './NoSettingsData'
import Organizations            from './Organizations'
import Plans                    from './Plans'
import handleOrganizationChange from './handleOrganizationChange'
import handlePlanChange         from './handlePlanChange'
import stateProps               from './stateProps'

// Module

class Navigator extends Component {
  render() {
    const {
            hostnames,
            organizations,
          } = this.props

    if (  !hostnames.alphaSuite.length
       || !hostnames.omegaSuite.length
       || !hostnames.orchestration.length
       )
      return <NoSettingsData />

    if (!Object.keys(organizations).length) return <NoOrchestrationData />

    return <div>
      <Grid container>
        <Grid item xs={12}>
          <Organizations />
        </Grid>
        <Grid item xs={12}>
          <Plans />
        </Grid>
      </Grid>
      <Health />
      <Controls />
    </div>
  }
}

export default connect(stateProps)(Navigator)
