// External Dependencies

import SettingsIcon from '@material-ui/icons/Settings'
import SyncIcon     from '@material-ui/icons/Sync'
import { connect }  from 'react-redux'

import {
  IconButton,
  Tooltip,
} from '@material-ui/core'

import React, {
  Component,
} from 'react'

// Internal Dependencies

import stateProps from './stateProps'

import { synchronize } from '@/messages'

// Module

class Controls extends Component {
  render() {
    const { orchestration } = this.props.hostnames

    return <div style={{position: 'absolute', right: 10, bottom: 10}}>
      <Tooltip
        title='Settings'
        arrow
      >
        <IconButton onClick={() => chrome.runtime.openOptionsPage()}>
          <SettingsIcon />
        </IconButton>
      </Tooltip>
      <Tooltip
        title='Synchronize with Orchestration'
        arrow
      >
        <IconButton onClick={() => synchronize(orchestration)}>
          <SyncIcon />
        </IconButton>
      </Tooltip>
    </div>
  }
}

export default connect(stateProps)(Controls)
