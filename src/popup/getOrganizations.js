// Internal Dependencies

import store                from 'p/store'
import { setOrganizations } from 'p/actions'

// Module

export default () =>
  chrome.storage.local.get(
    ['organizations'],
    ({organizations}) => {
      store.dispatch(setOrganizations(organizations || []))
    },
  )
