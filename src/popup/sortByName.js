// Module

export default data =>
  Object
    .entries(data)
    .sort(
      ([keyA, itemA], [keyB, itemB]) => itemA.name.localeCompare(itemB.name)
    )
