// Interal Dependencies

import getOrganizations   from './getOrganizations'
import getHostnames       from './getHostnames'
import getUrl             from './getUrl'
import listenForMessages  from './listenForMessages'
import listenForUrlChange from './listenForUrlChange'
import render             from './render'

// Module

getOrganizations()
getHostnames()
getUrl()
listenForUrlChange()
listenForMessages()
render()
