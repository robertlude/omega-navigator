// Internal Dependencies

import actionHandlers from './actionHandlers'

import createReducer from '@/createReducer'

// Module

export default createReducer(
  actionHandlers,
  {
    hostnames: {
      alphaSuite:    '',
      omegaSuite:    '',
      orchestration: '',
    },
  },
)
