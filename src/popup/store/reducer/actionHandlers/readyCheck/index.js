// Internal Dependencies

import getStateAttributes from './getStateAttributes'

import store             from 'p/store'
import { setAppVersion } from 'p/actions'

// Module

export default state => {
  if (!state.organizations || !state.url)
    return {
      ...state,
      ready: false,
    }

  const result = {
          ...state,
          ...getStateAttributes(state),
          ready: true,
        }

  if (result.health && state.appName != result.appName)
    fetch(result.health)
      .then(response => response.json())
      .then(({version}) => store.dispatch(setAppVersion(version)))

  return result
}
