// Internal Dependencies

import fallback     from './fallback'
import organization from './organization'
import plan         from './plan'

// Module

export default [
  organization,
  plan,

  fallback,
]
