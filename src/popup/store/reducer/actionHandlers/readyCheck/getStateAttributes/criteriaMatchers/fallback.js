// Internal Dependencies

import getFirstAlphabetically from 'p/getFirstAlphabetically'

// Module

export default ({organizations}) => {
  if (!Object.keys(organizations).length) return {}

  const organization = getFirstAlphabetically(organizations)

      , plan = getFirstAlphabetically(organization.plans)

  return {
    found: true,
    data: {
      oliveApp:       false,
      organizationId: organization.id,
      planId:         plan.id,
    }
  }
}
