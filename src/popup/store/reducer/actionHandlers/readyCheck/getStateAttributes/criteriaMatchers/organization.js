// Internal Dependencies

import locations from 'p/locations'

// Module

export default ({hostnames, organizations, url}) => {
  for (let location of locations.organizations) {
    const match = location.getIds(hostnames, url, organizations)

    if (match.found) return match
  }

  return {found: false}
}
