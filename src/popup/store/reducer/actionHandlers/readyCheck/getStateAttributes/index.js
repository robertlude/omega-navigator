// Internal Dependencies

import criteriaMatchers from './criteriaMatchers'

// Module

export default state => {
  for (let matcher of criteriaMatchers) {
    let match = matcher(state)

    if (match.found) return match.data
  }
}
