// Internal Dependencies

import getFirstAlphabetically from 'p/getFirstAlphabetically'

// Module

export default (state, {organizationId}) => {
  const organization = state.organizations[organizationId]

      , planId = getFirstAlphabetically(organization.plans).id

  return {
    ...state,
    organizationId,
    planId,
  }
}
