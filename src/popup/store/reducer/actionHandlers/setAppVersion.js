// Module

export default (state, {appVersion}) => ({
  ...state,
  appVersion,
})
