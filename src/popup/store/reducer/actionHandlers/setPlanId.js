// Module

export default (state, {planId}) => ({
  ...state,
  planId,
})
