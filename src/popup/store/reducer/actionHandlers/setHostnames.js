// Internal Dependencies

import readyCheck from './readyCheck'

// Module

const normalize = hostname => new URL(hostname).toString()

export default (state, {hostnames}) => {
  const normalizedHostnames = Object
                                .entries(hostnames)
                                .reduce(
                                  (result, [key, hostname]) => {
                                    result[key] = new URL(hostname).toString()

                                    return result
                                  },
                                  {},
                                )

  return readyCheck({
    ...state,
    hostnames: normalizedHostnames,
  })
}
