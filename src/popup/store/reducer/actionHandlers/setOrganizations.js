// Internal Dependencies

import readyCheck from './readyCheck'

// Module

export default (state, {organizations}) =>
  readyCheck({
    ...state,
    organizations,
  })
