// Internal Dependencies

import setAppVersion     from './setAppVersion'
import setHostnames      from './setHostnames'
import setOrganizationId from './setOrganizationId'
import setOrganizations  from './setOrganizations'
import setPlanId         from './setPlanId'
import setUrl            from './setUrl'

import {
  ACTION_TYPE__SET_APP_VERSION,
  ACTION_TYPE__SET_HOSTNAMES,
  ACTION_TYPE__SET_ORGANIZATION_ID,
  ACTION_TYPE__SET_ORGANIZATIONS,
  ACTION_TYPE__SET_PLAN_ID,
  ACTION_TYPE__SET_URL,
} from 'p/actionTypes'

// Module

export default {
  [ACTION_TYPE__SET_APP_VERSION]:     setAppVersion,
  [ACTION_TYPE__SET_HOSTNAMES]:       setHostnames,
  [ACTION_TYPE__SET_ORGANIZATION_ID]: setOrganizationId,
  [ACTION_TYPE__SET_ORGANIZATIONS]:   setOrganizations,
  [ACTION_TYPE__SET_PLAN_ID]:         setPlanId,
  [ACTION_TYPE__SET_URL]:             setUrl,
}
