// Internal Dependencies

import readyCheck from './readyCheck'

// Module

export default (state, {url}) =>
  readyCheck({
    ...state,
    url,
  })
