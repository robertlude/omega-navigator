// Module

const ACTION_TYPE__SET_APP_VERSION     = 'actionType:setAppVersion'
    , ACTION_TYPE__SET_HOSTNAMES       = 'actionType:setHostnames'
    , ACTION_TYPE__SET_ORGANIZATIONS   = 'actionType:setOrganizations'
    , ACTION_TYPE__SET_ORGANIZATION_ID = 'actionType:setOrganizationId'
    , ACTION_TYPE__SET_PLAN_ID         = 'actionType:setPlanId'
    , ACTION_TYPE__SET_URL             = 'actionType:setUrl'

export {
  ACTION_TYPE__SET_APP_VERSION,
  ACTION_TYPE__SET_HOSTNAMES,
  ACTION_TYPE__SET_ORGANIZATIONS,
  ACTION_TYPE__SET_ORGANIZATION_ID,
  ACTION_TYPE__SET_PLAN_ID,
  ACTION_TYPE__SET_URL,
}
