// Internal Dependencies

import { ACTION_TYPE__SET_ORGANIZATIONS } from 'p/actionTypes'

// Module

export default organizations => ({
  type: ACTION_TYPE__SET_ORGANIZATIONS,
  organizations,
})
