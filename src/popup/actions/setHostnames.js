// Internal Dependencies

import { ACTION_TYPE__SET_HOSTNAMES } from 'p/actionTypes'

// Module

export default hostnames => ({
  type: ACTION_TYPE__SET_HOSTNAMES,
  hostnames,
})
