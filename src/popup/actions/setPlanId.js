// Internal Dependencies

import { ACTION_TYPE__SET_PLAN_ID } from 'p/actionTypes'

// Module

export default planId => ({
  type: ACTION_TYPE__SET_PLAN_ID,
  planId,
})
