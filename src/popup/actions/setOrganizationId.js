// Internal Dependencies

import { ACTION_TYPE__SET_ORGANIZATION_ID } from 'p/actionTypes'

// Module

export default organizationId => ({
  type: ACTION_TYPE__SET_ORGANIZATION_ID,
  organizationId,
})
