// Internal Dependencies

import { ACTION_TYPE__SET_URL } from 'p/actionTypes'

// Module

export default url => ({
  type: ACTION_TYPE__SET_URL,
  url,
})
