// Internal Dependencies

import { ACTION_TYPE__SET_APP_VERSION } from 'p/actionTypes'

// Module

export default appVersion => ({
  type: ACTION_TYPE__SET_APP_VERSION,
  appVersion,
})
