// Internal Dependencies

import messageHandlers from 'p/messageHandlers'
import store           from 'p/store'

// Module

export default () =>
  chrome.runtime.onMessage.addListener(message => {
    messageHandlers[message.type](store, message)
  })
