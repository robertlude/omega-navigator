// Internal Dependencies

import sortByName from './sortByName'

// Module

export default list => sortByName(list)[0][1]
