// Internal Dependencies

import createOverlay      from './createOverlay'
import getHostname        from './getHostname'
import fetchOrganizations from './fetchOrganizations'
import fetchPlans         from './fetchPlans'

import { synchronized } from '@/messages'

// Module

const data = {
  hostname:      '',
  organizations: [],
}

createOverlay(data)
  .then(() => getHostname(data))
  .then(() => fetchOrganizations(data))
  .then(() => fetchPlans(data))
  .then(() => {
    console.log('All plan data fetched', data)

    data.overlay.updateText('Complete')

    synchronized(data.organizations)

    window.close()
  })
