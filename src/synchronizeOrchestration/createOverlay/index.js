// Internal Dependencies

import createStyleAttribute from './createStyleAttribute'

// Module

export default data => {
  const overlay          = document.createElement('div')
      , overlayText      = document.createElement('div')
      , overlayStyle     = {
                             'position':  'absolute',
                             'left':      0,
                             'right':     0,
                             'top':       0,
                             'bottom':    0,
                             'z-index':   10000000000,

                             'backdrop-filter': 'blur(3px)',
                             'background':      'rgba(255, 255, 255, 0.381966)',
                             'color':           '#000',
                             'font-size':       '36pt',
                             'text-shadow':     '0 0 5px #fff',
                           }
      , overlayTextStyle = {
                             'position':  'absolute',
                             'top':       '50%',
                             'left':      '10%',
                             'right':     '10%',
                             'transform': 'translate(0, -50%)',

                             'text-align': 'center',
                           }

      , overlayStyleAttribute     = createStyleAttribute(overlayStyle)
      , overlayTextStyleAttribute = createStyleAttribute(overlayTextStyle)


  overlayText.textContent = 'Please wait'
  overlayText.setAttribute('style', overlayTextStyleAttribute)

  overlay.setAttribute('style', overlayStyleAttribute)
  overlay.appendChild(overlayText)

  document.body.appendChild(overlay)

  data.overlay = { updateText: text => overlayText.textContent = text }

  return Promise.resolve()
}
