// Module

export default style => Object
                          .entries(style)
                          .map(([key, value]) => `${key}: ${value}`)
                          .join('; ')
