// Module

const fetchPlan = (data, organization, remaining) => new Promise((resolve, reject) => {
  const options = {
                    body:           null,
                    credentials:    'include',
                    method:         'GET',
                    mode:           'cors',
                    referrer:       `${data.hostname}plans/${organization.id}/list`,
                    referrerPolicy: 'no-referrer-when-downgrade',
                    headers: {
                      'accept':          'application/json',
                      'accept-language': 'en-US,en;q=0.9',
                      'sec-fetch-mode':  'cors',
                      'sec-fetch-site':  'same-origin'
                    },
                  }
      , url     = `${data.hostname}manager/api/plans`
                + `?organizationId=${organization.id}`

  data.overlay.updateText(`Fetching plans for ${organization.name}...`)

  fetch(url, options)
    .then(response => response.json())
    .then(plans => {
      organization.plans = plans.reduce(
        (planMap, plan) => {
          planMap[plan.id] = plan

          return planMap
        },
        {},
      )

      if (remaining.length) {
        const nextOrganization = remaining.shift()

        fetchPlan(data, nextOrganization, remaining)
          .then(resolve)
      } else {
        resolve()
      }
    })
  .catch(error => console.log('fetchPlan error', error))
})

export default fetchPlan
