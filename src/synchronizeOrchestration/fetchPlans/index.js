// Internal Dependencies

import fetchPlan from './fetchPlan'

// Module

export default data => new Promise((resolve, reject) => {
  const remaining = Object.values(data.organizations)

      , organization = remaining.shift()

  fetchPlan(data, organization, remaining).then(resolve)
})
