// Module

export default data => new Promise((resolve, reject) => {
  const url = new URL(window.location.href)

  url.pathname = ''

  data.hostname = url.toString()

  resolve()
})
