// Module

export default data => new Promise((resolve, reject) => {
  const options = {
                    body:           null,
                    cache:          'reload',
                    credentials:    'include',
                    method:         'GET',
                    mode:           'cors',
                    referrer:       `${data.hostname}plans`,
                    referrerPolicy: 'no-referrer-when-downgrade',
                    headers: {
                      'accept':          'application/json',
                      'accept-language': 'en-US,en;q=0.9',
                      'if-none-match':   'W/\"195d-tF3AVWbJ6IzGAThIWuEu6cZpHU8\"',
                      'sec-fetch-mode':  'cors',
                      'sec-fetch-site':  'same-origin',
                    },
                  }
      , url     = `${data.hostname}manager/api/public/organizations/permissions`
                + `?action=read&resource=plan`

  console.log({url})

  data.overlay.updateText('Fetching organizations...')

  fetch(url, options)
    .then(response => response.json())
    .then(organizations => {
      data.organizations = organizations.reduce(
        (organizationMap, organization) => {
          organizationMap[organization.id] = organization

          return organizationMap
        },
        {},
      )

      resolve()
    })
    .catch(error => console.log('fetchOrganizations error', error))
})
