# Omega Navigator

## Installation

In the entensions tab in Chrome, enable developer mode. Install the extension
from the `dist` directory in this repo.

## Updating

Perform a `git pull` on your local copy of the repository, and then click reload
in the extensions tab in Chrome.

## Usage

Select an organization and/or a plan from the drop-down menus.

* Clicking an action with no keys down will open the action in the current tab
* Holding command will open the action in a new tab
* Holding shift will open the action in a new window

## Development

### Preparation

```sh
npm install
```

### Automatic Building During Development

```sh
npm run watch
```

### Manual Building

```sh
npm run build
```

### Testing

Find the extension in the extensions tab in Chrome, and reload the extension to
test your changes.
