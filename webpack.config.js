const CopyWebpackPlugin = require('copy-webpack-plugin')
    , path              = require('path')

    , dist             = (file = '') => path.resolve(__dirname, 'dist', file)
    , src              = (file = '') => path.resolve(__dirname, 'src', file)
    , commonAttributes = {
                           devtool: 'inline-source-map',
                           mode:    'development',
                           module: {
                             rules: [
                               {
                                 exclude: /node_modules/,
                                 test:    /\.jsx?$/,
                                 use: {
                                   loader: 'babel-loader'
                                 },
                               },
                             ],
                           },
                           resolve: {
                             alias: {
                               '@': src(),
                               'p': src('popup'),
                               's': src('settings'),
                             },
                           },
                         }

    , copyFile = file => ({
                   from: src(file),
                   to:   dist(file),
                 })

    , copy = files => {
               const copyArray = []

               for (let index = 0; index < files.length; ++index) {
                 copyArray.push(copyFile(files[index]))
               }

               return new CopyWebpackPlugin(copyArray)
             }

module.exports = [
  // Pop-up code

  {
    ...commonAttributes,
    entry: './src/popup/index.js',
    output: {
      filename: 'popup.js',
      path:     dist(),
    },
    plugins: [
      copy([
        'img/omega-navigator-16.png',
        'img/omega-navigator-48.png',
        'img/omega-navigator-128.png',
        'manifest.json',
        'popup.html',
      ]),
    ],
  },

  // Background code

  {
    ...commonAttributes,
    entry: './src/background/index.js',
    output: {
      filename: 'background.js',
      path:     dist(),
    }
  },

  // Synchronization code

  {
    ...commonAttributes,
    entry: './src/synchronizeOrchestration/index.js',
    output: {
      filename: 'synchronizeOrchestration.js',
      path:     dist(),
    },
  },

  // Settings code

  {
    ...commonAttributes,
    entry: './src/settings/index.js',
    output: {
      filename: 'settings.js',
      path:     dist(),
    },
    plugins: [
      copy(['settings.html']),
    ]
  }
]
